<?php

class classMail {
    public static function get_content($args = null) {
        $content = "Message from: {{name_surname}}"."<br>".PHP_EOL;
        $content .= "Country: {{country}}"."<br>".PHP_EOL;
        $content .= "Email: {{e_mail}}"."<br>".PHP_EOL;
        $content .= "Trade: {{trade}}"."<br>".PHP_EOL;
        $content .= "Phone: {{phone}}"."<br><br>".PHP_EOL;
        $content .= "Attached message:".PHP_EOL."{{message}}".PHP_EOL;
        if ($args) {
            foreach ($args as $k => $v) {
                $content = str_replace("{{".$k."}}",$v,$content);
            }
        }
        return $content;
    }
    public static function get_reg_content($args = null) {
        $content = "Wiadomość od: {{name_surname}}"."<br>".PHP_EOL;
        $content .= "Email: {{e_mail}}"."<br>".PHP_EOL;
        $content .= "Telefon: {{phone}}"."<br><br>".PHP_EOL;
        if ($args) {
            foreach ($args as $k => $v) {
                $content = str_replace("{{".$k."}}",$v,$content);
            }
        }
        return $content;
    }
    public static function queue_mail($email, $action, $args = null, $links = null, $attachments = array()) {

        self::send_mail($email, $action, $args, $links);
    }
    public static function send_contact_mail($args = null, $send_to = null) {

        $from = get_field('smtp_from','options');
        $from_name = get_field('smtp_from_name','options');
        $host = get_field('smtp_host','options');
        $username = get_field('smtp_username','options');
        $password = get_field('smtp_password','options');
        $port = get_field('smtp_port','options');
        $content = self::get_content($args);
        $subject = get_field('contact_mail_subject','options');
        $emailbody = $content;
        //Phpmailer:
        $mail = new PHPMailer;
        $mail->CharSet = 'UTF-8';
        $mail->isSMTP();
        $mail->Host = $host;
        $mail->SMTPAuth = true;
        $mail->Username = $username;
        $mail->Password = $password;
        $mail->SMTPSecure = 'tls';
        $mail->Port = $port;
        $mail->From = $from;
        $mail->FromName = $from_name;
        $mail->addAddress($send_to);
        $mail->isHTML(true);
        $mail->Subject = $subject;
        $template = $emailbody;
        $mail->Body = $template;
        $mail->send();
    }
    public static function send_confirm_mail($send_to = null) {
        $from = get_field('smtp_from','options');
        $from_name = get_field('smtp_from_name','options');
        $host = get_field('smtp_host','options');
        $username = get_field('smtp_username','options');
        $password = get_field('smtp_password','options');
        $port = get_field('smtp_port','options');
        $subject = get_field('contact_mail_confirm_subject','options');
        $content = get_field('contact_mail_confirm_content','options');

        $emailbody = $content;
        //Phpmailer:
        $mail = new PHPMailer;
        $mail->CharSet = 'UTF-8';
        $mail->isSMTP();                                        // Set mailer to use SMTP
        $mail->Host = $host;  				    // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                                 // Enable SMTP authentication
        $mail->Username = $username;                // SMTP username
        $mail->Password = $password;             // SMTP password
        $mail->SMTPSecure = 'tls';                              // Enable TLS encryption, `ssl` also accepted
        $mail->Port = $port;                                      // TCP port to connect to
        $mail->From = $from;
        $mail->FromName = $from_name;
        $mail->addAddress($send_to);               // Name is optional
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $subject;
        $template = $emailbody;
        $mail->Body = $template;
        $mail->send();
    }
    public static function send_reg_mail($args = null,$send_to = null) {
        $from = get_field('smtp_from','options');
        $from_name = get_field('smtp_from_name','options');
        $host = get_field('smtp_host','options');
        $username = get_field('smtp_username','options');
        $password = get_field('smtp_password','options');
        $port = get_field('smtp_port','options');
        $subject = get_field('quick_reg_subject','options');
        $content = self::get_reg_content($args);
        $emailbody = $content;
        //Phpmailer:
        $mail = new PHPMailer;
        $mail->CharSet = 'UTF-8';
        $mail->isSMTP();                                        // Set mailer to use SMTP
        $mail->Host = $host;  				    // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                                 // Enable SMTP authentication
        $mail->Username = $username;                // SMTP username
        $mail->Password = $password;             // SMTP password
        $mail->SMTPSecure = 'tls';                              // Enable TLS encryption, `ssl` also accepted
        $mail->Port = $port;                                      // TCP port to connect to
        $mail->From = $from;
        $mail->FromName = $from_name;
        $mail->addAddress($send_to);               // Name is optional
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $subject;
        $template = $emailbody;
        $mail->Body = $template;
        $mail->send();
    }
}