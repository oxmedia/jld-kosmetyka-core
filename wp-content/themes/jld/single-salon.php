<?php get_header();?>
<?php
	$montofri = get_field('montofri',$post->ID);
	$sat = get_field('sobota',$post->ID);
	$sun = get_field('niedziela',$post->ID);
	$phone = get_field('phone',$post->ID);

	$thumb_id =  get_post_meta( $post->ID, '_thumbnail_id', true );
	$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'salon-big', true);
	$thumb_url = $thumb_url_array[0];

?>
<section id="single-salon">
	<div class="row">
		<div class="large-12 columns top-image" style="background-image: url('<?=$thumb_url;?>');;">
			<div class="address-box large-4 columns">
				<h3>
					<?=get_field('name',$post->ID);?>
					<br><?=get_field('street',$post->ID);?>
					<span><?=get_field('titleaddon',$post->ID);?></span>
				</h3>
				<ul>
					<?php if($montofri): ?>
						<li>
							<div class="left-col">pn - pt</div>
							<div class="right-col"><?=$montofri;?></div>
						</li>
					<?php endif;?>
					<?php if($sat): ?>
						<li>
							<div class="left-col">sobota</div>
							<div class="right-col"><?=$sat;?></div>
						</li>
					<?php endif;?>
					<?php if($sun): ?>
						<li>
							<div class="left-col">niedziela</div>
							<div class="right-col"><?=$sun;?></div>
						</li>
					<?php endif;?>
					<?php if($phone):?>
						<li class="phone">
						<div class="left-col"><i><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256.5 256.5" enable-background="new 0 0 256.5 256.5"><path d="m108.6 148c25.4 25.4 54.8 49.7 66.4 38 16.6-16.6 26.9-31.1 63.6-1.7 36.7 29.5 8.5 49.1-7.6 65.3-18.6 18.6-87.9 1-156.5-67.5-68.5-68.6-86.2-137.9-67.6-156.5 16.2-16.1 35.8-44.3 65.3-7.6 29.5 36.7 15 46.9-1.6 63.6-11.7 11.6 12.6 41 38 66.4"/></svg></i></div>
						<div class="right-col"><?=$phone;?></div>
					</li>
					<?php endif;?>
				</ul>
				<span class="clearfix"></span>
				<a class="btn red" href="<?=get_field('register_visit',$post->ID);?>">ZAREZERWUJ WIZYTĘ</a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="large-6 medium-9 columns small-centered">
			<h2 class="sectionHeader">SPRAWDŹ CENY W SALONIE
				<span class="under-header-line">
					<i class="left"></i>
					<i class="right"></i>
				</span>
			</h2>
			<p class="sectionSubHeader">Pamiętaj, że z <a href="<?=get_bloginfo('url');?>/karta-stalego-klienta/">Kartą Stałego Klienta</a> możesz otrzymać 20% rabatu</p>
		</div>
	</div>
	<div class="row">
		<div class="large-8 medium-12 columns">
			<div class="accordion">
                <?php $services = get_field('services');?>
                <?php $cats =  get_terms('services_category')?>
                <?php foreach($cats as $cat):?>

                    <h3>
                        <a href="#">
                            <figure><img src="<?=get_template_directory_uri();?>/images/accordion-<?=$cat->slug;?>.png" width="137" height="65"></figure>
                            <p class="accordion-label"><?=$cat->name;?></p>
                            <i></i>
                        </a>
                    </h3>
                    <div class="pane">
                        <ul>
                            <?php foreach($services as $service):?>
	                            <?php
                                $pterms = Array();
                                    $terms = wp_get_post_terms( $service->ID, 'services_category' );
                                    foreach($terms as $term):
	                                    $pterms[] = $term->slug;
                                    endforeach;
                                ?>
                                <?php if (in_array($cat->slug, $pterms)):?>
                                <li><a href="<?=get_permalink($service->ID);?>"><p class="text"><?=$service->post_title;?></p><span><?=get_field('service-price',$service->ID);?> zł</span></a></li>
                                <?php endif;?>
                            <?php endforeach;?>
                        </ul>
                    </div>

                <?php endforeach;?>
			</div>
			<?php if(get_field('link-fryzjerskie',$post->ID)):?>
			<div class="row">
				<div class="large-12 after-accordion columns">
					<p>W salonie oferujemy również usługi fryzjerskie. Aby zobaczyć ich cennik kliknij <a class="red" href="<?=get_field('link-fryzjerskie',$post->ID);?>">tutaj</a></p>
				</div>
			</div>
			<?php endif?>
		</div>
		<div class="large-4 medium-12 columns text-center">
			<div class="register-visit-sidebar">
				<p>skorzystaj z naszych usług<br>w tym salonie</p>
				<span class="clearfix"></span>
				<a class="btn red" href="<?=get_field('register_visit',$post->ID);?>">ZAREZERWUJ WIZYTĘ</a>
			</div>
			<div class="newsletter-sidebar">
				<p>Bądź na bieżąco z promocjami</p>
                <a href="<?=get_field('newsletter_external','option');?>" target="_blank" class="btn red newsletter-button">Zapisz się na newsletter</a>
			</div>

		</div>
	</div>
</section>


<?php get_footer();?>
