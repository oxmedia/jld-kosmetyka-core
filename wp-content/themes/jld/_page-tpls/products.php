<?php
/**
 * Template Name: Products
 */
get_header();
?>
<?php
    $desc = get_field('description',$post->ID);
?>
    <section id="product-list">
        <div class="row">
            <div class="large-12 columns text-center">
                <h2 class="sectionHeader"><?=get_field('header',$post->ID);?>
                    <span class="under-header-line">
                        <i class="left"></i>
                        <i class="right"></i>
                    </span>
                </h2>
            </div>
        </div>
        <?php if($desc):?>
        <div class="row">
            <div class="large-12 columns">
                <p class="sectionSubHeader"><?=$desc;?></p>
            </div>
        </div>
        <?php endif;?>
        <div class="row">
            <?php
                $products = get_posts(array('post_type'=>'products', 'posts_per_page'=>-1, 'products_category'=>get_field('selected_category',$post->ID)->slug));

            ?>
            <?php foreach($products as $product):?>

                <div class="small-12 medium-6 large-4 columns product-block end">
                    <a href="<?=get_permalink($product->ID);?>" title="<?=$product->post_title;?>" class="product">
                        <figure>
	                        <?php
	                            $thumb_id =  get_post_meta( $product->ID, '_thumbnail_id', true );
	                            $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'product-listing', true);
	                            $thumb_url = $thumb_url_array[0];
	                        ?>
                            <img src="<?=$thumb_url;?>" alt="<?=$product->post_title;?>">
                        </figure>
                        <div class="title">
	                        <?php
	                        $x = 40;
	                        $longString = $product->post_title;
	                        $lines = explode("\n", wordwrap($longString, $x));

	                        ?>
	                        <?php foreach($lines as $line):?>

                                <div class="text-row">
                                    <span><?=$line;?></span>
                                </div>
	                        <?php endforeach;?>
                        </div>
                        <p class="price"><?=get_field('product-price',$product->ID);?></p>
                    </a>
                </div>


            <?php endforeach;?>
        </div>
        <?php
            $visible = get_field('banner_visible',$post->ID);
            $desc = get_field('banner_description',$post->ID);
            $bg = get_field('banner_image',$post->ID);
        ?>
        <?php if($visible):?>
        <div class="row">
            <div class="large-12 columns">
                <div class="banner" <?php if($bg):?>style="background-image:url('<?=$bg;?>')"<?php endif;?>>
                    <?php if($desc):?>
                        <p><?=$desc;?></p>
                    <?php else:?>
                        <p>Wszystkie produkty można zakupić<br>w salonach kosmetycznych Jean Louis David</p>
                    <?php endif;?>
                </div>
            </div>
        </div>
        <?php endif;?>
    </section>
<?php get_footer();?>