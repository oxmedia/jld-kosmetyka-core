<?php
/**
 * Template Name: Salony
 */
get_header(); ?>

    <section id="map" ng-controller="mapsCtrl">
        <div class="row">
            <h2 class="large-push-3 medium-push-1 large-6 medium-10 small-12 columns sectionHeader">
                ZNAJDŹ NAJBLIŻSZY SALON
                <span class="under-header-line">
                    <i class="left"></i>
                    <i class="right"></i>
            </span>
            </h2>
        </div>
		<?php include(locate_template('_partials/mapbox.php'));?>
    </section>

<?php get_footer();?>