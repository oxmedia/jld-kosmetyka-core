<?php
/**
 * Created by PhpStorm.
 * User: WebKoala
 * Date: 14.11.2016
 * Time: 10:43
 */


function jld_scripts() {


	wp_register_style( 'main', get_template_directory_uri() .'/styles/main.css', array(), RC_random_string());
	wp_enqueue_style( 'main' );
	wp_register_style( 'vendor', get_template_directory_uri() .'/styles/vendor.css', array(), RC_random_string());
	wp_enqueue_style( 'vendor' );
	wp_register_script('main', get_template_directory_uri() . '/scripts/main.min.js', array('vendor'), RC_random_string(), true);
	wp_enqueue_script('main');
	wp_register_script('vendor', get_template_directory_uri() . '/scripts/vendor.js', array(), RC_random_string(), true);
	wp_enqueue_script('vendor');
	wp_register_script('ngsanitize', get_template_directory_uri() . '/scripts/ngsanitize.js', array('vendor'), RC_random_string(), true);
	wp_enqueue_script('ngsanitize');
	wp_register_script('wp-main', get_template_directory_uri() . '/scripts/main-wp.js', array(), RC_random_string(), true);
	wp_enqueue_script('wp-main');
	wp_enqueue_script( 'gmaps', 'https://maps.google.com/maps/api/js?key=AIzaSyCaiX4FMtZuxko9YFlUTPc30lN33zpNQb0', array('main','vendor'), RC_random_string());
	wp_enqueue_script( 'gmaps' );
	wp_register_script('maps', get_template_directory_uri() . '/scripts/maps.min.js', array('vendor','gmaps'), RC_random_string(), true);
	wp_enqueue_script('maps');

	wp_localize_script('main', 'adminajax', array(
		'ajaxurl' => admin_url('admin-ajax.php'),
		'homeurl' => home_url(),
		'templateurl' => get_template_directory_uri()
	));
}
add_action( 'wp_enqueue_scripts', 'jld_scripts' );


