<?php


class classSessionHandler {

    public static function set($key, $value)
    {
        $name = self::constructSessionVariableName($key);
        $_SESSION[$name] = $value;
    }

    public static function get($key, $default = false)
    {
        $name = self::constructSessionVariableName($key);
        return isset($_SESSION[$name]) ? $_SESSION[$name] : $default;
    }

    public static function clear($key)
    {
        $name = self::constructSessionVariableName($key);
        if (isset($_SESSION[$name])) {
            unset($_SESSION[$name]);
        }
    }
	
	public static function get_and_clear($key)
    {
        $name = self::constructSessionVariableName($key);
        if (isset($_SESSION[$name])) {
			$ret = $_SESSION[$name];
            unset($_SESSION[$name]);
			return $ret;
        }
    }

//    public static function clearAll()
//    {
//        foreach ($_SESSION as $key=>$v) {
//            unset($_SESSION[$key]);
//        }
//    }
    
    public static function exists($key){
            $name = self::constructSessionVariableName($key);
            return isset($_SESSION[$name]);
    }
    
    protected static function constructSessionVariableName($key)
    {
        return 'sh_'.$key;
    }
}
