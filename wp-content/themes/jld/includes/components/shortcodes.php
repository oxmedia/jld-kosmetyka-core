<?php

/* 
 * Plik w którym dodajemy shortcody.
 * Poniżej szablony do kopiowania

	function shortcode1($atts, $content = '') {
		extract(shortcode_atts(array(
			'attribute1' => '',
			'attribute2' => '',
		), $atts));

		return '';
	}
	add_shortcode('shortcode1', 'shortcode1');

	function shortcode2($atts, $content = '') {
		return '';
	}
	add_shortcode('shortcode2', 'shortcode2');

 * 
 * 
 */

function red_text( $attr, $content = null ) {
	return '<span class="red">' . $content . '</span>';
}

add_shortcode( 'red', 'red_text' );