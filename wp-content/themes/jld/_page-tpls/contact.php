<?php
/**
 * Template Name: Contact
 */
get_header();
?>
    <div id="contact-page">
        <div class="row">
            <div class="large-12 columns text-center">
                <h2 class="sectionHeader">
				    <?=get_field('header',$post->ID);?>
                    <span class="under-header-line">
                      <i class="left"></i>
                      <i class="right"></i>
                    </span>
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="large-12 columns">
                <p class="sectionSubHeader">
				    <?=get_field('subheader',$post->ID);?>
                </p>
            </div>
        </div>
    </div>
    <div id="single-text">
        <div class="row">
            <div class="medium-6 columns">
                <?=get_field('lewy_box',$post->ID);?>
            </div>
            <div class="medium-6 columns">
	            <?=get_field('prawy_box',$post->ID);?>
            </div>
        </div>
    </div>
<?php
$map =get_field('mapa',$post->ID);
?>
<?php if($map):?>
    <div id="contact-map" style="min-height:400px;" data-lat="<?=$map['lat'];?>" data-lng="<?=$map['lng'];?>"></div>
<?php endif;?>
<?php get_footer();?>