<div class="row">
    <div class="large-8 large-push-2 medium-10 medium-push-1 small-10 small-push-1 columns">
        <div class="row">
            <div class="large-6 columns filter-label">
                Wpisz  miasto, ulicę, nazwe centrum handlowego<br>
                albo zobacz <a href="#">listę wszystkich naszych salonów</a>
            </div>
            <div class="large-6 columns">
                <div class="field-wrapper">
                    <input type="text" placeholder="Wpisz nazwę miasta" ng-model="searchText">
                    <button>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14.5 14.5"><path d="M1327.1 1248.68a1.336 1.336 0 0 1 .4 .96 1.358 1.358 0 0 1 -2.32 .96l-3.98-3.98a5.421 5.421 0 1 1 1.92 -1.92l3.98 3.98m-8.66-10.82a4.08 4.08 0 1 0 4.08 4.08A4.078 4.078 0 0 0 1318.44 1237.86" transform="translate(-1313-1236.5)" style="fill-rule:evenodd"></path></svg>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="map-row">
    <div class="row">
        <div class="large-12 columns">
            <ul class="list-switch no-margin">
                <li ng-click="setType('map');">
                    <a class="map" ng-class="{'active':place_type=='map'}">
                        <svg x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" enable-background="new 0 0 512 512" xml:space="preserve"><path d="M352,32l-192,96L0,32v352l160,96l192-96l160,96V128L352,32z M176,155.781l160-80v280.438l-160,80V155.781z M32,88.5
	l111.531,66.938l0.469,0.281v277.375L32,365.875V88.5z M480,423.469l-111.531-66.906L368,356.281V78.938l112,67.188V423.469z"></path></svg>
                        MAPA
                    </a>
                </li>
                <li ng-click="setType('list');">
                    <a class="list" ng-class="{'active':place_type=='list'}">
                        <svg width="16px" height="12px" viewBox="-1 -1 16 12" version="1.1">
                            <g id="list" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="square">
                                <path d="M0.636363636,1 L13.3636364,1" id="Line" stroke-width="2"></path>
                                <path d="M0.636363636,5 L13.3636364,5" id="Line" stroke-width="2"></path>
                                <path d="M0.636363636,9 L13.3636364,9" id="Line" stroke-width="2"></path>
                            </g>
                        </svg>LISTA</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="row" ng-show="place_type==='map'" style="position: relative">
        <div id="box-{{$index}}" class="box fryzjer" data-group="fryzjer" ng-repeat="point in points | filter:searchText">
            <div class="left-col">
                <span ng-if="point.cosmetic" class="label kosmetyczny">kosmetyczny</span>
                <span ng-if="point.barber" class="label fryzjerski">fryzjerski</span>
                <ul class="adres">
                    <li>{{point.city}}</li>
                    <li>{{point.street}}</li>
                    <li><span style="font-size: 10px; line-height: 12px" ng-bind-html="point.floor"></span></li>
                </ul>
                <ul class="godziny">
                    <li><span>pn - pt</span>{{point.open.weekend}}</li>
                    <li><span>sobota</span>{{point.open.sobota}}</li>
                    <li><span>niedziela</span>{{point.open.niedziela}}</li>
                </ul>
                <p class="telefon">
                    <i><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256.5 256.5" enable-background="new 0 0 256.5 256.5"><path d="m108.6 148c25.4 25.4 54.8 49.7 66.4 38 16.6-16.6 26.9-31.1 63.6-1.7 36.7 29.5 8.5 49.1-7.6 65.3-18.6 18.6-87.9 1-156.5-67.5-68.5-68.6-86.2-137.9-67.6-156.5 16.2-16.1 35.8-44.3 65.3-7.6 29.5 36.7 15 46.9-1.6 63.6-11.7 11.6 12.6 41 38 66.4"/></svg></i>{{point.phone}}
                </p>
                <a class="btn red" href="{{point.permalink}}">zobacz cennik</a>
            </div>
            <div class="right-col" style="padding-bottom: 20px; overflow: hidden">
                <h4>Dostępność usług w salonie</h4>
                <ul ng-repeat="servicelist in point.services">

                        <li ng-repeat="service in servicelist">{{service.name}}</li>
                </ul>
            </div>
            <i class="triangle"></i>
            <a class="close" ng-click="hidePopups();"></a>
        </div>
        <div id="bigMap" class="object-map" style="margin-top:0"></div>
    </div>
    <div class="row" ng-show="place_type==='list'">

        <div class="object large-12 columns" ng-repeat="point in points | filter:searchText">
            <div class="object-infos">
                <div class="medium-6 large-3 columns adres">
                    {{point.name}}<br>
                    {{point.street}}<br>
                    {{point.postcode }} {{point.city}}
                </div>
                <div class="medium-6 large-3 columns godziny">
                    <ul>
                        <li class="left-col">pn - pt</li>
                        <li class="right-col">{{point.open.weekend}}</li>
                        <li class="left-col">sobota</li>
                        <li class="right-col">{{point.open.sobota}}</li>
                        <li class="left-col">niedziela</li>
                        <li class="right-col">{{point.open.niedziela}}</li>
                    </ul>

                </div>
                <div class="medium-4 large-2 columns telefon">
                    <i></i>
                    {{point.phone}}
                </div>
                <div class="medium-4 large-2 columns dostepnosc">
                    <a ng-click="point.active = !point.active">Zobacz dostępność usług</a>
                </div>
                <div class="medium-4 large-2 columns cennik" ng-class="{'show':point.active}">
                    <a class="btn red" href="{{point.permalink}}">zobacz cennik</a>
                </div>
                <div class="large-12 columns object-services" ng-class="{'active':point.active}">
                    <ul class="large-3 columns end" ng-repeat="chunk in point.services">
                        <li ng-class="{'disabled':c.status}" ng-repeat="c in chunk">{{c.name}}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    #map .box .left-col .adres li span br {display: none;}
</style>