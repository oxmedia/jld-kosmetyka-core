<?php

class classUser{
	
	public static function init(){
	}
	
	public static function standard_login($username,$pass,$remember){
		$creds = array();
		$creds['user_login'] 	= sanitize_user($username);
		$creds['user_password'] = $pass;
		$creds['remember'] 		= esc_attr($remember);
		
		$result = wp_signon($creds);
		return $result;
	}
	
	public static function standard_register($username,$pass,$first_name='',$last_name='',$company=''){
		$userdata = array(
			'user_login'  =>  sanitize_user($username),
			'user_pass'   =>  $pass,
			'user_email'  =>  sanitize_user($username),
			'first_name'  =>  $first_name,
			'last_name'  =>  $last_name,
			'display_name' => $first_name.' '.$last_name
			
		);
		$result = wp_insert_user($userdata);
		if(!is_wp_error($result)){
			update_user_meta($result,'user_company',$company);
			update_user_meta($result,'linkedin_user',0);
			$creds = array();
			$creds['user_login'] 	= sanitize_user($username);
			$creds['user_password'] = $pass;
			$creds['remember'] 		= false;
			$logresult = wp_signon($creds);
			if($logresult){
				wp_safe_redirect('/profil/');
				exit();
			}
		}
		return $result;
	}
	
	private static function getRandomBytes($nbBytes = 32)
	{
		$bytes = openssl_random_pseudo_bytes($nbBytes, $strong);
		if (false !== $bytes && true === $strong) {
			return $bytes;
		}
		else {
			throw new Exception("Nie można stworzyć bezpiecznego klucza.");
		}
	}
	
	public static function generate_random_password($length){
		
		return substr(preg_replace("/[^a-zA-Z0-9]/", "", base64_encode(self::getRandomBytes($length+1))),0,$length);
		
	}
	
	public static function get_logout_link(){
		if(is_user_logged_in()){
			echo '/wyloguj/';
		}
	}

}

