<?php


class classRequire {
   
   static $instance;
    private $post;
    private $get;
    private $file;
    private $isAddslashesOn;

    public function __construct() {
        $this->get = &$_GET;
        $this->post = &$_POST;
        $this->file = &$_FILES;
        $this->isAddslashesOn = get_magic_quotes_gpc();
        //$this->protection($this->get);
        $this->protection($this->post);

        foreach ($this->post as $key => $val) {
            if ($val == null) {
                $this->post[$key] = '';
            }
        }
    }

    public static function isPost() {
        if (!isset(self::$instance)) {
            self::$instance = new classRequire;
        }


        if (count(self::$instance->post) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static function isFile() {
        if (!isset(self::$instance)) {
            self::$instance = new classRequire;
        }

        if (count(self::$instance->file) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static function getPost($key, $optional = null) {

        if (!isset(self::$instance)) {
            self::$instance = new classRequire;
        }

        if (isset(self::$instance->post[$key])) {
            return self::$instance->post[$key];
        } else {
            return $optional;
        }
    }

    public static function getPosts($optional = null) {

        if (!isset(self::$instance)) {
            self::$instance = new classRequire;
        }

        if (isset(self::$instance->post)) {
            return self::$instance->post;
        } else {
            return $optional;
        }
    }

    public static function getFile($key, $optional = null) {

        if (!isset(self::$instance)) {
            self::$instance = new classRequire;
        }

        if (isset(self::$instance->file[$key])) {
            return self::$instance->file[$key];
        } else {
            return $optional;
        }
    }

    public static function getGet($key, $optional = null) {

        if (!isset(self::$instance)) {
            self::$instance = new classRequire;
        }

        if (isset(self::$instance->get[$key])) {
            return self::$instance->get[$key];
        } else {
            return $optional;
        }
    }

    public static function setGet($key, $value) {

        if (!isset(self::$instance)) {
            self::$instance = new classRequire;
        }

        self::$instance->get[$key] = $value;
    }

    private function protection(&$array) {

        foreach ($array as $k => &$v) {
            if (is_array($v))
                $this->protection($v);
            else {
                $v = trim($v);
            }
        }
    }
    
    
}
