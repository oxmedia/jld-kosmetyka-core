<?php

class classFrontendHelpers {
    public static function menuFindIfActivePage($menu_item){
        global $post;
        $post_ID = $post->ID;
        if($menu_item->object_id == $post_ID){
            return true;
        }
        if(!empty($menu_item->child_menu_items)){
            foreach($menu_item->child_menu_items as $child_item){
                if($child_item->object_id == $post_ID){
                    return true;
                }
            }
        }
        return false;
    }
}
