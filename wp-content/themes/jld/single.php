<?php get_header(); ?>

    <div class="row">
        <div class="large-12 columns text-center">
            <h1 class="sectionHeader"><?=$post->post_title;?>
                <span class="under-header-line">
                        <i class="left"></i>
                        <i class="right"></i>
                    </span>
            </h1>
        </div>
    </div>
    <div id="single-text">
        <div class="row">
            <article class="large-12 columns">
                <?=apply_filters('the_content',$post->post_content);?>
            </article>
        </div>

    </div>

<?php get_footer() ?>