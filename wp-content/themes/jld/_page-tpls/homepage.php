<?php
/**
 * Template Name: Home Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header();
?>
    <section id="homepage-headers">
        <?php
            $header = get_field('headers_header',$post->ID);
            $subheader = get_field('headers_subheader',$post->ID);
            $headers_btn_target = get_field('headers_btn_target',$post->ID);
            $headers_btn_label = get_field('headers_btn_label',$post->ID);
        ?>

        <?php if($header):?>
          <div class="row">
            <div class="large-12 columns">
              <h2 class="sectionHeader"><?=$header;?>
                <span class="under-header-line">
                <i class="left"></i>
                <i class="right"></i>
              </span>
              </h2>
            </div>
          </div>
	    <?php endif;?>
        <?php if($subheader):?>
            <div class="row">
                <div class="large-12 columns">
                    <p><?=$subheader?></p>
                </div>
            </div>
        <?php endif;?>
        <?php if($headers_btn_label && $headers_btn_target):?>
            <div class="row">
                <div class="large-12 columns">
                    <a class="btn red clearfix" target="_blank" href="https://rezerwacjeonline-jeanlouisdavid.pl/" title="<?=$headers_btn_label;?>"><?=$headers_btn_label;?></a>
                </div>
            </div>
        <?php endif;?>
    </section>
    <section id="homepage-promotions">
      <div class="row">
        <div class="large-12 columns text-center">
            <h2 class="sectionHeader">
                <?=get_field('promo_header',$post->ID);?>
                <span class="under-header-line">
                <i class="left"></i>
                <i class="right"></i>
              </span>
            </h2>
        </div>
      </div>
      <div class="row collapse">
        <div class="promotions-carousel">
          <div class="slides owl-carousel">
              <?php foreach(get_posts(array('post_type'=>'promotions', 'posts_per_page'=>-1)) as $promo):?>

                  <div>
                      <a href="<?=get_permalink($promo->ID);?>" title="<?=$promo->post_title;?>">
                          <figure>
                              <?php if(get_field('ribbon',$promo->ID)):?>
                                <i class="ribbon"></i>
                              <?php endif;?>
			                  <?php
                                $thumb_id =  get_post_meta( $promo->ID, '_thumbnail_id', true );
                                $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'promo-listing', true);
                                $thumb_url = $thumb_url_array[0];
			                  ?>
                              <img src="<?=$thumb_url;?>" alt="<?=$promo->post_title;?>">
                          </figure>
                          <div class="title">

	                          <?php
	                          $x = 40;
	                          $longString = $promo->post_title;
	                          $lines = explode("\n", wordwrap($longString, $x));

	                          ?>
                              <?php foreach($lines as $line):?>

                                  <div class="text-row">
                                      <span><?=$line;?></span>
                                  </div>
                                <?php endforeach;?>
                          </div>
                      </a>
                  </div>

              <?php endforeach;?>
          </div>
          <div class="nav-container"></div>
          <div class="dots-container"></div>
        </div>
      </div>
    </section>
    <section id="map" ng-controller="mapsCtrl">
        <div class="row">
            <h2 class="large-push-3 medium-push-1 large-6 medium-10 small-12 columns sectionHeader">
                ZNAJDŹ NAJBLIŻSZY SALON
                <span class="under-header-line">
                    <i class="left"></i>
                    <i class="right"></i>
            </span>
            </h2>
        </div>
		<?php include(locate_template('_partials/mapbox.php'));?>
    </section>
    <section id="news">
      <div class="row">
        <div class="news-list">
          <div class="row">
            <div class="small-centered large-4 medium-5 small-11 column">
              <h2 class="sectionHeader">Aktualności
                <span class="under-header-line">
                  <i class="left"></i>
                  <i class="right"></i>
                </span>
              </h2>
            </div>
          </div>
          <div class="row" id="newsContainer">
              <ul class="left"></ul>
              <ul class="mid"></ul>
              <ul class="right"></ul>
          </div>
            <div class="row">
                <div class="large-12 columns text-center">
                    <div class="spinner" id="news-spinner" style="display: none">
                        <div class="bounce1"></div>
                        <div class="bounce2"></div>
                        <div class="bounce3"></div>
                    </div>
                </div>
            </div>
          <div class="large-12 columns clearfix">

            <button class="btn" id="loadMoreNews" data-offset="0">pokaż więcej</button>
          </div>
        </div>




      </div>


    </section>

    <?php $content = get_the_content(); ?>
    <?php if ($content): ?>
        <section>
            <div class="row">
                <div class="small-12 columns seo-text-box">
                    <?php echo wpautop($content); ?>
                </div>
            </div>
        </section>
    <?php endif; ?>
<?php get_footer();?>