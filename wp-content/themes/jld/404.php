<?php

/**
 * Created by PhpStorm.
 * User: WebKoala
 * Date: 18.11.2016
 * Time: 08:58
 */
get_header();
$newsletter = true;
$contact = true;
$pagecol = 'yellow';
?>

<div class="row">
    <div class="error404-section">
        <div class="left">
            <h1 class="firstText">ups!</h1>
            <h2 class="secoundText">Coś poszło nie tak...</h2>
            <div class="button-holder">
                <a href="/" class="btn red clearfix">Wróć na stronę główną</a>
            </div>
        </div>
        <div class="right">
            <img src="<?=get_template_directory_uri();?>/images/desk.gif" alt="404">
        </div>
    </div>
</div>
<?php

include( locate_template( 'footer.php' ) );

?>