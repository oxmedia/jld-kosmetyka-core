<?php /* Template Name: Blog */ ?>
<?php get_header(); ?>

<div class="row">
    <div class="large-12 columns text-center">
        <h1 class="sectionHeader">Blog
            <span class="under-header-line">
                <i class="left"></i>
                <i class="right"></i>
            </span>
        </h1>
    </div>
</div>

<?php

$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

$args = [
    'post_type' => 'blog',
    'posts_per_page' => 3,
    'paged' => $paged,
];
$query = new WP_Query($args);

global $wp_query;
$tmp_query = $wp_query;
$wp_query = null;
$wp_query = $query;

?>
<div class="row">
    <div class="small-12 column">
        <?php
        if ( have_posts() ) :
            while (have_posts()) : the_post();
                ?>
                <article class="blog-post">
                    <div class="article-date"><?php echo get_the_date('d/m/Y'); ?></div>
                    <h2 class="article-title"><a href="<?php echo esc_url(get_permalink()); ?>"><?php the_title(); ?></a></h2>
                    <?php if (has_post_thumbnail() && ( get_theme_mod('index_feat_image') != 1 )) : ?>
                        <div class="article-thumb">
                            <?php the_post_thumbnail(); ?>
                        </div>
                    <?php endif; ?>
                    <div class="article-text">
                        <?php the_excerpt(); ?>
                    </div>
                    <a class="article-read-more" href="<?php echo esc_url(get_permalink()); ?>">Czytaj więcej</a>
                </article>
            <?php endwhile; ?>
        <?php else: ?>
            <p>Brak wpisów.</p>
        <?php endif; ?>
    </div>
</div>


<div class="row">
    <div class="small-12 column">
        <div class="posts-navigation">
            <div class="button-holder"><?php previous_posts_link('Nowsze posty'); ?></div>
            <div class="button-holder"><?php next_posts_link('Starsze posty'); ?></div>
        </div>
    </div>
</div>

<?php
$wp_query = null;
$wp_query = $tmp_query
?>

<?php get_footer() ?>
