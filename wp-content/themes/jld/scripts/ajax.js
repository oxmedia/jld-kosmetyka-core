'use strict';

// Load more blog posts or portfolio items
window.jld._loadMore = function _loadMore(type, offset, callback) {
	if (typeof callback !== 'function') {
		callback = false;
	}

	setTimeout(function () {
		$.ajax({
			type: "POST",
			url: 'http://localhost/~kamilf/load_more.php', //build:remove
			//build:uncomment url: window.location.protocol+'//'+window.location.host+'/load_more.php',
			data: { 'post-type': type, 'offset': offset },
			success: function success(res) {
				//result pure HTML
				callback(res);
			},
			error: function error() {
				callback('error');
			}
		});
	}, 2000);
};

// Load team member info
window.jld._teamMember = function _teamMember(id, callback) {
	if (typeof callback !== 'function') {
		callback = false;
	}

	setTimeout(function () {
		$.ajax({
			type: "POST",
			url: 'http://localhost/~kamilf/load_more.php', //build:remove
			//build:uncomment url: window.location.protocol+'//'+window.location.host+'/load_more.php',
			data: { 'member': id },
			success: function success(res) {
				//result  JSON
				var response = JSON.parse(res);
				var _res = { memberImage: response.memberImage, memberContent: response.memberContent };
				callback(_res);
			},
			error: function error() {
				callback('error');
			}
		});
	}, 2000);
};

//Send contact form to mailchamp
window.jld._sendContact = function _sendContact(form, callback) {
	if (typeof callback !== 'function') {
		callback = false;
	}

	setTimeout(function () {
		$.ajax({
			type: "POST",
			url: 'http://localhost/~kamilf/mailchamp.php?action=contact', //build:remove
			//build:uncomment url: window.location.protocol+'//'+window.location.host+'/mailchamp.php?action=contact',
			data: $(form).serialize(),
			success: function success(res) {
				var response = JSON.parse(res);
				if (response.status) callback(true);else callback(false);
			},
			error: function error() {
				callback(false);
			}
		});
	}, 2000);
};

//Send newslleter subscribe form to mailchamp
window.jld._subscribe = function _subscribe(form, callback) {
	if (typeof callback !== 'function') {
		callback = false;
	}

	setTimeout(function () {
		$.ajax({
			type: "POST",
			url: 'http://localhost/~kamilf/mailchamp.php?action=subscribe', //build:remove
			//build:uncomment url: window.location.protocol+'//'+window.location.host+'/mailchamp.php?action=subscribe',
			data: $(form).serialize(),
			success: function success(res) {
				var response = JSON.parse(res);
				if (response.status) callback(true);else callback(false);
			},
			error: function error() {
				callback(false);
			}
		});
	}, 2000);
};
//# sourceMappingURL=ajax.js.map
