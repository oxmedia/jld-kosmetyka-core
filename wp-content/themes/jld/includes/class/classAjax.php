<?php


class classAjax
{
    public static function init()
    {

        $ajaxevents = array(
                            'contactEmail' => false,
                            'getsalons' => false,
                            'save_form_input' => false,
                            'save_form_reg' => false,
                            'save_form_fullreg' => false,
                            'get_member' => false,
                            'load_more' => false,
                            'get_post_count' => false,
                            'form' =>false,
                            'upload'=>false,
                            );

        foreach ($ajaxevents as $ajax => $priv) {
            add_action('wp_ajax_' . $ajax, array(__CLASS__, $ajax));
            if (!$priv) {
                add_action('wp_ajax_nopriv_' . $ajax, array(__CLASS__, $ajax));
            }
        }

    }
    public static function form () {
      $data = $_POST;

      $api_key = get_field('mailchimp_api','options');
      $list_id = get_field('mailchimp_list','options');

      $Mailchimp = new Mailchimp( $api_key );
      $Mailchimp_Lists = new Mailchimp_Lists( $Mailchimp );

      $email = $_POST['email'] ? htmlentities($_POST['email']) : '';
      $name = $_POST['first_name'] ? htmlentities($_POST['first_name']) : '';
      $surname = $_POST['first_name'] ? htmlentities($_POST['last_name']) : '';

      if($_POST['submittype'] == 'contact'){
       $message = htmlentities($_POST['message']);
       $phone = htmlentities($_POST['phone']);
       $who = htmlentities(implode($_POST['who'],', '));
       $attachment = in_array('Entrepreneur', $_POST['who']) ? htmlentities($_POST['attachment']) : '';
   }

   $subscriber = null;

		if($_POST['submittype'] == 'subscribe'){ //newsletter
			$subscriber = $Mailchimp_Lists->subscribe( $list_id, array( 'email' => $email), array('FNAME'=>$name, 'LNAME'=>$surname) );
		}else if($_POST['submittype'] == 'contact'){ //contact
			$subscriber = $Mailchimp_Lists->subscribe( $list_id, array( 'email' => $email), array('FNAME'=>$name, 'LNAME'=>$surname, 'WHOIS'=>$who, 'FILE' => $attachment, 'MSG' => $message, 'PHONE' => $phone) );
		}

		if ( ! empty( $subscriber['leid'] ) ) {
			echo json_encode(array('status'=>true));
		}
		else
		{
			echo json_encode(array('status'=>false));
		}
		die();

	}
    public static function contactEmail () {
        $data = $_POST['details'];


        die();

    }
    public static function getsalons () {

        $salons = get_posts(array('post_type'=>'salon','posts_per_page'=>-1));

        $salonslist = array();

        foreach($salons as $salon):

	        $thumb_id =  get_post_meta( $salon->ID, '_thumbnail_id', true );
	        $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'salon-big', true);
	        $thumb_url = $thumb_url_array[0];


	        $x = new stdClass();
            $x->id = $salon->ID;
            $x->active = false;
            $x->name = $salon->post_title;
            $x->street = get_field('street',$salon->ID);
            $x->postCode = get_field('post-code',$salon->ID);
            $x->city = get_field('city',$salon->ID);
            $x->barber = get_field('fryzjerski',$salon->ID);
            $x->cosmetic = get_field('kosmetyczny',$salon->ID);
            $x->phone = get_field('phone',$salon->ID);
            $x->permalink = get_permalink($salon->ID);
            $x->floor = get_field('titleaddon',$salon->ID);
            $x->open->weekend = get_field('montofri',$salon->ID);
	        $x->open->sobota = get_field('sobota',$salon->ID);
	        $x->open->niedziela = get_field('niedziela',$salon->ID);
	        $x->register_visit = get_field('register_visit',$salon->ID);
	        $x->linkfryzjerskie = get_field('link-fryzjerskie',$salon->ID);
	        $x->register_visit = get_field('register_visit',$salon->ID);
            $ll = get_field('mapa',$salon->ID);
	        $x->lat = $ll['lng'];
	        $x->long = $ll['lat'];
            $x->img = $thumb_url;
	        $services = get_field('services',$salon->ID);
            $serviceslist = array();
            foreach($services as $s):
                $serviceslist[] = array('name'=>get_the_title($s));
            endforeach;
            $serviceslist = array_chunk((array)$serviceslist, round(count($serviceslist) / 3));
	        $x->services = $serviceslist;

            $salonslist[] = $x;

        endforeach;

        echo json_encode($salonslist);


        die();

    }
    public static function get_post_count(){
        $count = ['all' => 0];
        
        foreach (get_categories(array('hide_empty'=>false)) as $cat){
            $count[$cat->slug] = $cat->count;
            $count['all'] += $cat->count;
        }

        echo json_encode($count);
        die();
    }
    public static function load_more() {
        $data = $_POST;
        $offset = $data['offset'];
        $postType = 'post';


                $newslist = get_posts(array('post_type'=>'post','posts_per_page'=>6,'offset'=>$offset));
                $cols = 3;
                $chunksize = round(count($newslist) / $cols);
                $nchunks = array_chunk((array)$newslist,$chunksize);

                if(count($newslist) > 0):
                //Left col:
	            ob_start();
                $i=0; foreach ($nchunks[0] as $news):?>
			    <?php
			    $thumb_id = get_post_meta( $news->ID, '_thumbnail_id', true );
			    if($i % 2 ==0):
				    $size = 'small';
				    if($thumb_id):
					    $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'news-image-small', true);
					    $thumb_url = $thumb_url_array[0];
				    else:
					    $thumb_url = 'http://lorempixel.com/276/214/fashion/'.$i;
				    endif;
			    else:
				    $size = 'big';
				    if($thumb_id):
					    $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'news-image-big', true);
					    $thumb_url = $thumb_url_array[0];
				    else:
					    $thumb_url = 'http://lorempixel.com/276/318/fashion/'.$i;
				    endif;
			    endif;
			    ?>
                <li class="news <?=$size;?>">
                    <a href="<?=get_permalink($news->ID);?>">
                        <figure>
                            <img src="<?=$thumb_url;?>" alt="<?=$news->post_title;?>">
                        </figure>
                        <p class="title"><?=$news->post_title;?></p>
                        <p class="desc"><?=getExcerpt(strip_tags($news->post_excerpt),0,75);?></p>
                    </a>
                </li>
			    <?php $i++; endforeach;
	            $leftcol = ob_get_clean();
	            //Middle col:
	            ob_start();
	            $i=0; foreach ($nchunks[1] as $news):?>
			    <?php
	                $thumb_id = get_post_meta( $news->ID, '_thumbnail_id', true );
			    if($i % 2 ==0):
				    $size = 'big';
				    if($thumb_id):
					    $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'news-image-big', true);
					    $thumb_url = $thumb_url_array[0];
				    else:
					    $thumb_url = 'http://lorempixel.com/276/318/fashion/'.$i;
				    endif;
			    else:
				    $size = 'small';
				    if($thumb_id):
					    $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'news-image-small', true);
					    $thumb_url = $thumb_url_array[0];
				    else:
					    $thumb_url = 'http://lorempixel.com/276/214/fashion/'.$i;
				    endif;
			    endif;
			    ?>
                    <li class="news <?=$size;?>">
                        <a href="<?=get_permalink($news->ID);?>">
                            <figure>
                                <img src="<?=$thumb_url;?>" alt="<?=$news->post_title;?>">
                            </figure>
                            <p class="title"><?=$news->post_title;?></p>
                            <p class="desc"><?=getExcerpt(strip_tags($news->post_excerpt),0,75);?></p>
                        </a>
                    </li>
			    <?php $i++; endforeach;
	            $midcol = ob_get_clean();
	            //Right col
	            ob_start();
                $i=0; foreach ($nchunks[2] as $news):?>
			    <?php
	                $thumb_id = get_post_meta( $news->ID, '_thumbnail_id', true );
			    if($i % 2 ==0):
				    $size = 'small';
				    if($thumb_id):
					    $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'news-image-small', true);
					    $thumb_url = $thumb_url_array[0];
				    else:
					    $thumb_url = 'http://lorempixel.com/276/214/fashion/'.$i;
				    endif;
			    else:
				    $size = 'big';
				    if($thumb_id):
					    $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'news-image-big', true);
					    $thumb_url = $thumb_url_array[0];
				    else:
					    $thumb_url = 'http://lorempixel.com/276/318/fashion/'.$i;
				    endif;

			    endif;
			    ?>
                    <li class="news <?=$size;?>">
                        <a href="<?=get_permalink($news->ID);?>">
                            <figure>
                                <img src="<?=$thumb_url;?>" alt="<?=$news->post_title;?>">
                            </figure>
                            <p class="title"><?=$news->post_title;?></p>
                            <p class="desc"><?=getExcerpt(strip_tags($news->post_excerpt),0,75);?></p>
                        </a>
                    </li>
			    <?php $i++; endforeach;
	            $rightcol = ob_get_clean();

                echo json_encode([
                        'lcol' => $leftcol,
                        'rcol' => $rightcol,
                        'mcol' => $midcol,
                        'counter' =>count($newslist)
                ]);

                else:
                echo json_encode(['all' => true]);
                endif;

                die();
            }
    public static function upload () {
                $data = $_POST;
                var_dump($data,$_FILES);

                if(!is_dir(dirname( __FILE__ )."/attachments"))
                  mkdir(dirname( __FILE__ )."/attachments", 0777);

              if (!empty($_FILES)) {
                  $tempFile = $_FILES['file']['tmp_name'];
                  $file = pathinfo($_FILES['file']['name']);
                  $newName = date('d-m-Y')."_".rtrim($file['basename'], '.'.$file['extension'])."_".time().".".$file['extension'];
                  $targetFile =  dirname( __FILE__ )."/attachments/". $newName;
                  var_dump($targetFile);
                  if(move_uploaded_file($tempFile, $targetFile)){
                   echo json_encode(array('link' => 'http://'.$_SERVER['http_HOST']."/attachments/".$newName));
               }
           }

           die();

       }
    public static function save_form_input()
       {
        $return['result'] = true;
        $data = classRequire::getPost('data');
        if (!empty($data)) {
            $parsed_data = [];
            foreach ((array)$data as $d) {
                $parsed_data[$d['name']] = strip_tags($d['value']);
            }
            if (
                !empty($parsed_data['name']) &&
                !empty($parsed_data['email']) &&
                !empty($parsed_data['message']) &&
                !empty($parsed_data['clinic']) &&
                !empty($parsed_data['phone'])
                ) {

                $postarr = array(
                                 'post_title' => $parsed_data['name'] . ' (' . $parsed_data['email'] . ')',
                                 'post_type' => 'form_message',
                                 'post_status' => 'publish',
                                 'post_author' => 1,
                                 'meta_input' => [
                                 'name_surname' => $parsed_data['name'],
                                 'e_mail' => $parsed_data['email'],
                                 'phone' => $parsed_data['phone'],
                                 'message' => $parsed_data['message'],
//                        'regulations_check' => !empty($parsed_data['regulations_check']),
//                        'newsletter_check' => !empty($parsed_data['newsletter_check'])
                                 ]
                                 );
            $result = wp_insert_post($postarr);

            if ($result) {
                $return['result'] = true;
                classMail::send_contact_mail([
                                             'name_surname' => $parsed_data['name'],
                                             'e_mail' => $parsed_data['email'],
                                             'phone' => $parsed_data['phone'],
                                             'message' => $parsed_data['message']
                                             ], $parsed_data['clinic']);
                classMail::send_confirm_mail($parsed_data['email']);
            } else {
                $return['result'] = false;
            }
        } else {
            $return['result'] = false;
        }
    } else { 
        $return['result'] = false;
    }
    echo json_encode($return);
    exit();
}
    public static function save_form_reg()
{
    $return['result'] = true;
    $data = classRequire::getPost('data');
    if (!empty($data)) {
        $parsed_data = [];
        foreach ((array)$data as $d) {
            $parsed_data[$d['name']] = strip_tags($d['value']);
        }
        if (
            !empty($parsed_data['name']) &&
            !empty($parsed_data['email']) &&
            !empty($parsed_data['clinic']) &&
            !empty($parsed_data['phone'])
            ) {

            $postarr = array(
                             'post_title' => $parsed_data['name'] . ' (' . $parsed_data['email'] . ')',
                             'post_type' => 'registrations',
                             'post_status' => 'publish',
                             'post_author' => 1,
                             'meta_input' => [
                             'name_surname' => $parsed_data['name'],
                             'e_mail' => $parsed_data['email'],
                             'phone' => $parsed_data['phone'],
                             'clinic' => $parsed_data['clinic'],
//                        'regulations_check' => !empty($parsed_data['regulations_check']),
//                        'newsletter_check' => !empty($parsed_data['newsletter_check'])
                             ]
                             );
        $result = wp_insert_post($postarr);
        $category = get_term_by('slug', 'strona-glowna', 'registrations-category');
                $tag = array( $category->term_id ); // Correct. This will add the tag with the id 5.
                wp_set_post_terms( $result, $tag, 'registrations-category' );
                if ($result) {
                    $return['result'] = true;
                    classMail::send_reg_mail(
                                             [
                                             'name_surname' => $parsed_data['name'],
                                             'e_mail' => $parsed_data['email'],
                                             'phone' => $parsed_data['phone']
                                             ],
                                             $parsed_data['clinic']);
                } else {
                    $return['result'] = false;
                }
            } else {
                $return['result'] = false;
            }
        } else {
            $return['result'] = false;
        }
        echo json_encode($return);
        exit();
    }
    public static function save_form_callback()
    {
        $return['result'] = true;
        $data = classRequire::getPost('data');
        if (!empty($data)) {
            $parsed_data = [];
            foreach ((array)$data as $d) {
                $parsed_data[$d['name']] = strip_tags($d['value']);
            }
            if (
                !empty($parsed_data['phone'])
                ) {

                $postarr = array(
                                 'post_title' => $parsed_data['phone'],
                                 'post_type' => 'callbacks',
                                 'post_status' => 'publish',
                                 'post_author' => 1
                                 );
            $result = wp_insert_post($postarr);

            if ($result) {
                $return['result'] = true;
                classMail::send_reg_mail(
                                         [
                                         'phone' => $parsed_data['phone']
                                         ],
                                         $parsed_data['clinic']);
            } else {
                $return['result'] = false;
            }
        } else {
            $return['result'] = false;
        }
    } else {
        $return['result'] = false;
    }
    echo json_encode($return);
    exit();
}
    public static function save_form_fullreg()
{
    $return['result'] = true;
    $data = classRequire::getPost('data');

    if (!empty($data)) {
        $parsed_data = [];
        foreach ((array)$data as $d) {
            $parsed_data[$d['name']] = strip_tags($d['value']);
        }
        if (
            !empty($parsed_data['name']) &&
            !empty($parsed_data['email']) &&
            !empty($parsed_data['clinic']) &&
            !empty($parsed_data['phone'])
            ) {
            $postarr = array(
                             'post_title' => $parsed_data['name'] . ' (' . $parsed_data['email'] . ')',
                             'post_type' => 'registrations',
                             'post_status' => 'publish',
                             'post_author' => 1,
                             'meta_input' => [
                             'name_surname' => $parsed_data['name'],
                             'e_mail' => $parsed_data['email'],
                             'pesel' => $parsed_data['pesel'],
                             'address' => $parsed_data['address'],
                             'birthday' => $parsed_data['birthday'],
                             'phone' => $parsed_data['phone'],
                             'clinic' => $parsed_data['clinic'],
                             'attachements' => $parsed_data['attachedfiles'],
                             ]
                             );

        $result = wp_insert_post($postarr);
        $category = get_term_by('slug', 'formularz-rejestracyjny', 'registrations-category');
                $tag = array( $category->term_id ); // Correct. This will add the tag with the id 5.
                wp_set_post_terms( $result, $tag, 'registrations-category' );
                if ($result) {
                    $return['result'] = true;
                    classMail::send_reg_mail(
                                             [
                                             'name_surname' => $parsed_data['name'],
                                             'e_mail' => $parsed_data['email'],
                                             'phone' => $parsed_data['phone']
                                             ],
                                             $parsed_data['clinic']);
                } else {
                    $return['result'] = false;
                }
            } else {
                $return['result'] = false;
            }
        } else {
            $return['result'] = false;
        }
        echo json_encode($return);
        exit();
    }
}