<?php

define('TINC_DIR', __DIR__);

require_once TINC_DIR . "/usage/phpmailer/phpmailer/PHPMailerAutoload.php";
require_once TINC_DIR . "/usage/mc/Mailchimp.php";
require_once TINC_DIR . '/class/class-tgm-plugin-activation.php';
class classLoaderCust
{

    static public function loadClass($class_name)
    {
        if (!class_exists($class_name)) {

            if (preg_match('/^class/', $class_name)) {
                $path = __DIR__ . '/class/' . $class_name . '.php';

                if (isset($path) && file_exists($path)) {
                    require_once($path);
                } else {
                    echo('No class like ' . $class_name);
                    exit;
                }

            }

        }

    }


}

spl_autoload_register('classLoaderCust::loadClass');

$components = classDirectory::getFileList(__DIR__ . '/components/');
foreach ($components as $file) {
    require_once(__DIR__ . '/components/' . $file);
}
classAjax::init();
classFormHandler::init();
classUser::init();
