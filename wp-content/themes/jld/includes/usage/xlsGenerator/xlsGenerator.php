<?php

use SimpleExcel\SimpleExcel;

class xlsGenerator {
	
	private $write_data;
	
	public function __construct() {
		
	}
	
	
	public function addRecord($name, $email, $id, $payments,$date,$time, $table_size, $people, $traditional_menu, $alternative_menu, $phone = '', $additional_info = ''){
		$hash = md5($name.$email.$id);
		if($payments==1){
			$payments="Opłacone";
		}elseif($payments==='Status płatności'){
			
		}else{
			$payments="Nieopłacone"; 
		}
		$this->write_data[$hash] = array();
		
		$this->write_data[$hash]['id'] = $id;
		$this->write_data[$hash]['name'] = $name;
		$this->write_data[$hash]['email'] = $email;
		$this->write_data[$hash]['phone'] = $phone;
		$this->write_data[$hash]['payments'] = $payments;
		$this->write_data[$hash]['date'] = $date;
		$this->write_data[$hash]['time'] = $time;
		$this->write_data[$hash]['people'] = $people;
		$this->write_data[$hash]['table_size'] = $table_size;
		$this->write_data[$hash]['traditional_menu'] = $traditional_menu;
		$this->write_data[$hash]['alternative_menu'] = $alternative_menu;
		$this->write_data[$hash]['additional_info'] = $additional_info;
	}
	
	public function addEvRecord($name, $email, $id, $payments,$date,$time, $people, $phone = ''){
		$hash = md5($name.$email.$id);
		
		if($payments==1){
			$payments="Opłacone";
		}elseif($payments==='Status płatności'){
			
		}else{
			$payments="Nieopłacone"; 
		}
		$this->write_data[$hash] = array();
		
		$this->write_data[$hash]['id'] = $id;
		$this->write_data[$hash]['name'] = $name;
		$this->write_data[$hash]['email'] = $email;
		$this->write_data[$hash]['phone'] = $phone;
		$this->write_data[$hash]['payments'] = $payments;
		$this->write_data[$hash]['date'] = $date;
		$this->write_data[$hash]['time'] = $time;
		$this->write_data[$hash]['people'] = $people;
	}
	
	public function addUserRecord($login, $email,$display_name,$role){
		$hash = md5($login.$display_name.$email);
		
		$this->write_data[$hash] = array();
		
		$this->write_data[$hash]['login'] = $login;
		$this->write_data[$hash]['email'] = $email;
		$this->write_data[$hash]['display_name'] = $display_name;
		$this->write_data[$hash]['role'] = $role;
	}

	public function addTablesRecord($name,$os2a,$os2r,$os4a,$os4r,$os6a,$os6r,$os8a,$os8r,$maxTables){

		$hash = md5($name);
		
		$this->write_data[$hash] = array();
		
		$this->write_data[$hash]['name'] = $name;
		$this->write_data[$hash]['os2a'] = $os2a;
		$this->write_data[$hash]['os2r'] = $os2r;
		$this->write_data[$hash]['os4a'] = $os4a;
		$this->write_data[$hash]['os4r'] = $os4r;
		$this->write_data[$hash]['os6a'] = $os6a;
		$this->write_data[$hash]['os6r'] = $os6r;
		$this->write_data[$hash]['os8a'] = $os8a;
		$this->write_data[$hash]['os8r'] = $os8r;
		$this->write_data[$hash]['maxTables'] = $maxTables;
	}
	public function addPlacesRecord($name,$r,$mina,$maxa){
		$hash = md5($name);
		
		$this->write_data[$hash] = array();
		
		$this->write_data[$hash]['name'] = $name;
		$this->write_data[$hash]['r'] = $r;
		$this->write_data[$hash]['mina'] = $mina;
		$this->write_data[$hash]['maxa'] = $maxa;
	}
	public function addRatingsRecord($rname,$name,$r,$mina,$maxa){
		$hash = md5($name);
		
		$this->write_data[$hash] = array();
		
		$this->write_data[$hash]['rname'] = $rname;
		$this->write_data[$hash]['name'] = $name;
		$this->write_data[$hash]['r'] = $r;
		$this->write_data[$hash]['mina'] = $mina;
		$this->write_data[$hash]['maxa'] = $maxa;
	}
	public function addTupleRecord($name,$value){
		$hash = md5($name);
		
		$this->write_data[$hash] = array();
		
		$this->write_data[$hash]['name'] = $name;
		$this->write_data[$hash]['value'] = $value;
	}
	public function addRestaurantRecord($rname, $fname, $fnip, $facc , $fstreet, $fnr, $fpc, $fc){
		$hash = md5($rname.$fname);
		
		$this->write_data[$hash] = array();
		
		$this->write_data[$hash]['rname'] = $rname;
		$this->write_data[$hash]['fname'] = $fname;
		$this->write_data[$hash]['fnip'] = $fnip;
		$this->write_data[$hash]['facc'] = $facc;
		$this->write_data[$hash]['fstreet'] = $fstreet;
		$this->write_data[$hash]['fnr'] = $fnr;
		$this->write_data[$hash]['fpc'] = $fpc;
		$this->write_data[$hash]['fc'] = $fc;
	}
	
	public function addCustomRecord($args){
	
		$hash = md5(mt_rand());
		foreach((array)$args as $k=>$arg){
			$this->write_data[$hash][$k]=$arg;
		}
		
	}
	
	public function save($path){
		require_once(realpath(dirname(__FILE__)).'/src/SimpleExcel/SimpleExcel.php'); 

		$excel = new SimpleExcel('xml');
		$excel->writer->setData($this->write_data);
		$excel->writer->saveFile('example', $path);
	}

	public function download($name){

		require_once(realpath(dirname(__FILE__)).'/src/SimpleExcel/SimpleExcel.php'); 

		$excel = new SimpleExcel('xml');
		$excel->writer->setData($this->write_data);

		$excel->writer->saveFile($name, null, true);
	}	
	
	
}

