<?php
/**
 * Created by PhpStorm.
 * User: WebKoala
 * Date: 27.10.2016
 * Time: 15:07
 */


add_action( 'tgmpa_register', 'jld_plugins_register_required_plugins' );

function jld_plugins_register_required_plugins() {
	/*
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	$plugins = array(

		// This is an example of how to include a plugin bundled with a theme.
		array(
			'name'               => 'ACF Pro', // The plugin name.
			'slug'               => 'ACF Pro', // The plugin slug (typically the folder name).
			'source'             => get_template_directory() . '/includes/plugins/acf.zip', // The plugin source.
			'required'           => true, // If false, the plugin is only 'recommended' instead of required.
			'force_activation'   => true,
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
		),
		array(
			'name'      => 'Eggplant 301 Redirects',
			'slug'      => 'eps-301-redirects',
			'required'  => false,
		),
		array(
			'name'        => 'WordPress SEO by Yoast',
			'slug'        => 'wordpress-seo',
			'is_callable' => 'wpseo_init',
			'required'    => true,
		),
		array(
			'name'        => 'WP Smush',
			'slug'        => 'wp-smushit',
			'required'    => true,
		),
		array(
			'name'        => 'iThemes Security',
			'slug'        => 'better-wp-security',
			'required'    => true,
		),
		array(
			'name'        => 'W3 Total Cache',
			'slug'        => 'w3-total-cache',
			'required'    => false,
		),
		array(
			'name'        => 'TinyMCE Advanced',
			'slug'        => 'tinymce-advanced',
			'required'    => true,
		),
		array(
			'name'        => 'CMS Tree Page View',
			'slug'        => 'cms-tree-page-view',
			'required'    => false,
		),
		array(
			'name'        => 'ACF Keyword analysis for Yoast',
			'slug'        => 'acf-content-analysis-for-yoast-seo',
			'required'    => true,
		),
		array(
			'name'        => 'WP SMTP',
			'slug'        => 'wp-smtp',
			'required'    => false,
		),

	);
	$config = array(
		'id'           => 'jld',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'parent_slug'  => 'themes.php',            // Parent menu slug.
		'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.
	);

	tgmpa( $plugins, $config );
}
