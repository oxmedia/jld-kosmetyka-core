<footer id="footer">
    <div class="inner-footer row">
        <div class="medium-4 medium-push-8 columns appointment">
            <h6><?=get_field('footer_app_header','options');?></h6>
            <a class="red make-appointment" href="<?=get_field('footer_app_reg_link','options');?>">
                <i><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17 17" enable-background="new 0 0 17 17"><path d="m3 0h2v4h-2z"/><path d="m12 0h2v4h-2z"/><path d="M15,2v3h-4V2H6v3H2V2H0v4v10v1h1h16v-1V6V2H15z M16,16H1V6h1h4h5h4h1V16z"/><path d="m9 9h-1v2h-2v1h2v2h1v-2h2v-1h-2z"/></svg></i><?=get_field('footer_app_reg_label','options');?></a>
            <div class="call">
	            <?=get_field('footer_app_call_label','options');?>
                <span><i><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 80 80" enable-background="new 0 0 80 80"><path d="m46.1 46.1c-7.9 8-17 15.5-20.7 11.9-5.2-5.2-8.4-9.7-19.8-.5-11.4 9.2-2.6 15.3 2.4 20.3 5.8 5.8 27.4.3 48.8-21.1 21.3-21.3 26.8-42.9 21-48.7-5-5-11.2-13.8-20.3-2.4-9.2 11.4-4.7 14.6.5 19.8 3.6 3.7-3.9 12.8-11.9 20.7"/></svg></i>
	                <?=get_field('footer_app_call_phone','options');?>
                </span>
            </div>
        </div>
        <div class="medium-4 columns footer-nav">
            <h6><?=get_field('footer_nav_menu','options');?></h6>
            <ul>
	            <?php
	            wp_nav_menu(array(
		            'theme_location' => 'footer-menu',
		            'menu_class' => 'footer-1-menu-desktop',
		            'container'       => false,
	            ));
	            ?>
            </ul>

        </div>
        <div class="medium-4 columns medium-pull-8 newsletter">
            <h6><?=get_field('footer_newsletter_header','options');?></h6>
            <a href="<?=get_field('newsletter_external','option');?>" target="_blank" class="btn red newsletter-button">Zapisz się na newsletter</a>
        </div>
    </div>

</footer>
<?php wp_footer(); ?>
</body>
</html>