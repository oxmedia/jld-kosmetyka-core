<?php
/**
 * Template Name: Promos
 */
get_header();
?>
<?php
    $desc = get_field('description',$post->ID);
?>
    <section id="promo-list">
        <div class="row">
            <div class="large-12 columns text-center">
                <h2 class="sectionHeader"><?=get_field('header',$post->ID);?>
                    <span class="under-header-line">
                        <i class="left"></i>
                        <i class="right"></i>
                    </span>
                </h2>
            </div>
        </div>
        <?php if($desc):?>
        <div class="row">
            <div class="large-12 columns">
                <p class="sectionSubHeader"><?=$desc;?></p>
            </div>
        </div>
        <?php endif;?>
        <div class="row">
            <?php
                $products = get_posts(array('post_type'=>'promotions', 'posts_per_page'=>-1));

            ?>
            <?php foreach($products as $product):?>

                <div class="small-12 medium-6 large-4 columns">
                    <a href="<?=get_permalink($product->ID);?>" title="<?=$product->post_title;?>" class="product">
                        <figure>
	                        <?php
	                        $thumb_id =  get_post_meta( $product->ID, '_thumbnail_id', true );
	                        $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'promo-listing', true);
	                        $thumb_url = $thumb_url_array[0];
	                        ?>
                            <img src="<?=$thumb_url;?>" alt="<?=$product->post_title;?>">
                        </figure>
                        <div class="title">
	                        <?php
	                        $x = 40;
	                        $longString = $product->post_title;
	                        $lines = explode("\n", wordwrap($longString, $x));

	                        ?>
	                        <?php foreach($lines as $line):?>

                                <div class="text-row">
                                    <span><?=$line;?></span>
                                </div>
	                        <?php endforeach;?>
                        </div>
                    </a>
                </div>


            <?php endforeach;?>
        </div>
    </section>
<?php get_footer();?>