<?php get_header();?>
    <div id="single-service">
    <div class="row">
        <div class="large-12 columns text-center">
            <h1 class="service-title"><?=$post->post_title;?></h1>
        </div>
    </div>
    <div class="row first-row">
        <div class="medium-6 large-4 columns">
            <div class="service-carousel owl-carousel">
                <?php foreach(get_field('service-slider',$post->ID) as $slider):?>
                    <div class="item"><figure><img src="<?=$slider['image']['sizes']['service-slide'];?>" alt="<?=$post->post_title;?>"></figure></div>
                <?php endforeach;?>
            </div>
            <div class="nav-container"></div>
            <div class="dots-container"></div>
        </div>
        <div class="medium-6 large-4 columns large-push-4" >
            <div class="price-list">
                <p class="title">CENNIK</p>
                <?php
                $pricelist = get_field('lista_cen',$post->ID);
                    if(count($pricelist)>0):
                ?>
                <ul>
                    <?php foreach ($pricelist as $price):?>
                    <li><span><?=$price['nazwa'];?>  </span><strong><?=$price['cena'];?> zł</strong></li>
                    <?php endforeach;?>
                </ul>
                <p class="title">skorzystaj z usług w tym salonie</p>
				
                <a target="_blank" href="https://rezerwacje-jeanlouisdavid.pl/jld/reservation" class="btn" style="padding: 0;height: 46px;width: 231px;line-height: 46px;text-align: center;">ZAREZERWUJ WIZYTĘ</a>
                <?php else:?>

                <p>Skontaktuj się z nami w celu ustalenia cen</p>

                <?php endif;?>

            </div>
        </div>
        <div class="medium-12 large-4 columns large-pull-4">
            <div class="description">
                <?=get_field('service-description',$post->ID);?>
            </div>
        </div>
    </div>


    <?php
        $cb = get_field('content_builder',$post->ID);
    ?>

    <?php foreach($cb as $block):?>

        <?php if($block['acf_fc_layout']==='why'):?>

            <section id="why">
                <?php if(isset($block['why-header'])):?>
                <div class="row">
                    <div class="large-12 text-center columns">
                        <h2 class="sectionHeader"><?=$block['why-header'];?>
                            <span class="under-header-line">
                              <i class="left"></i>
                              <i class="right"></i>
                            </span>
                        </h2>
                    </div>
                </div>
                <?php endif;?>
                <div class="row collapse">
                    <div class="large-12 columns">
                        <div class="service-why owl-carousel">
                            <?php foreach($block['why-blocks'] as $b):?>
                            <div>
                                <figure>
                                    <img src="<?=$b['why-block-image']['sizes']['why-image'];?>" alt="">
                                </figure>
                                <div class="title">

		                            <?php
		                            $x = 40;
		                            $longString = $b['why-block-title'];
		                            $lines = explode("\n", wordwrap($longString, $x));

		                            ?>
		                            <?php foreach($lines as $line):?>

                                        <div class="text-row">
                                            <span><?=$line;?></span>
                                        </div>
		                            <?php endforeach;?>
                                </div>
                                <p><?=$b['tekst'];?></p>
                            </div>
                            <?php endforeach;?>
                        </div>
                        <div class="nav-container-why"></div>
                        <div class="dots-container-why"></div>
                    </div>
                </div>
            </section>

        <?php elseif($block['acf_fc_layout']==='preparation'):?>

            <section id="preparation">
                <div class="row">
                    <div class="large-12 columns text-center">
                        <h2 class="sectionHeader"><?=$block['preparation-header'];?>
                            <span class="under-header-line">
                              <i class="left"></i>
                              <i class="right"></i>
                            </span>
                        </h2>
                    </div>
                </div>
                <div class="row">
                    <ul class="small-block-grid-1 medium-block-grid-2 large-block-grid-4">
                        <?php $i=1; foreach($block['preparation-circles'] as $circle):?>
                        <li>
                            <div class="circle circle-<?=$i;?>">
                                <div class="circle-content">
                                    <p><?=$circle['preparation-circle-text'];?></p>
                                </div>
                            </div>
                            <div class="content">
                                <ul>
                                    <?php foreach($circle['preparation-list'] as $listitem):?>
                                    <li><?=$listitem['preparation-list-text'];?></li>
                                    <?php endforeach;?>
                                </ul>
                            </div>
                        </li>
                        <?php $i++; endforeach;?>
                    </ul>
                </div>
                <?php if(isset($block['preparation-btn-label'])):?>
                <div class="row">
                    <div class="large-12 columns text-center">
                        <a href="<?=$block['preparation-pdf']['url'];?>" target="_blank" class="btn red"><?=$block['preparation-btn-label'];?></a>
                    </div>
                </div>
                <?php endif;?>
            </section>

	    <?php elseif($block['acf_fc_layout']==='device'):?>

            <section id="device">
                <div class="row">
                    <div class="large-12 text-center columns">
                        <h2 class="sectionHeader"><?=$block['device-header'];?>
                            <span class="under-header-line">
                              <i class="left"></i>
                              <i class="right"></i>
                            </span>
                        </h2>
                    </div>
                </div>
                <div class="row content" data-equalizer>
                <?php if($block['device-image']):?>
                    <div class="large-4 columns large-push-8" data-equalizer-watch>
                        <img src="<?=$block['device-image'];?>" alt="<?=$block['device-header'];?>">
                    </div>

                    <div class="large-8 large-pull-4 columns" data-equalizer-watch>
	                    <?=$block['device-text'];?>
                    </div>


                <?php else:?>

                    <div class="large-8 large-push-2 columns" data-equalizer-watch>
		                <?=$block['device-text'];?>
                    </div>
                <?php endif;?>

                </div>
            </section>

        <?php elseif($block['acf_fc_layout']==='related-services'):?>

            <section id="similiar-services">
                <div class="row text-center">
                    <h2 class="sectionHeader">
                        <?=$block['related-services-header'];?>
                        <span class="under-header-line">
                            <i class="left"></i>
                            <i class="right"></i>
                        </span>
                    </h2>
                </div>
                <div class="row collapse">
                    <div class="services-carousel">
                        <div class="slides owl-carousel">
                            <?php foreach($block['related-services-list'] as $rp):?>

                                <div>
                                    <a href="<?=get_permalink($rp);?>" title="<?=get_the_title($rp);?>">
                                        <figure>
	                                        <?php
	                                        $thumb_id =  get_post_meta( $rp, '_thumbnail_id', true );
	                                        $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'promo-listing', true);
	                                        $thumb_url = $thumb_url_array[0];
	                                        ?>
                                            <img src="<?=$thumb_url;?>" alt="<?=get_the_title($rp);?>">
                                        </figure>
                                        <div class="title">

		                                    <?php
		                                    $x = 40;
		                                    $longString = get_the_title($rp);
		                                    $lines = explode("\n", wordwrap($longString, $x));

		                                    ?>
		                                    <?php foreach($lines as $line):?>

                                                <div class="text-row">
                                                    <span><?=$line;?></span>
                                                </div>
		                                    <?php endforeach;?>
                                        </div>
                                    </a>

                                </div>

                            <?php endforeach;?>


                        </div>
                        <div class="nav-container"></div>
                        <div class="dots-container"></div>
                    </div>
                </div>
            </section>

	    <?php elseif($block['acf_fc_layout']==='contraindications'):?>

            <section id="contraindications">
                <div class="row">
                    <div class="large-12 text-center columns">
                        <h2 class="sectionHeader"><?=$block['contraindications-header'];?>
                            <span class="under-header-line">
                              <i class="left"></i>
                              <i class="right"></i>
                            </span>
                        </h2>
                    </div>
                </div>
                <div class="row">
                    <div class="large-12 columns">
                        <ul>
                            <?php foreach($block['contraindications-list'] as $lisitem):?>
                            <li><?=$lisitem['tekst'];?></li>
                            <?php endforeach;?>
                        </ul>
                    </div>
                </div>
            </section>

	    <?php elseif($block['acf_fc_layout']==='reservation'):?>

            <section id="register">
                <div class="row">
                    <div class="large-12 text-center columns register-content" style="background-image: url('<?= $block['reservation-image']; ?>');">
                        <h2 class="sectionHeader"><?=$block['reservation-header'];?>
                            <span class="under-header-line">
                              <i class="left"></i>
                              <i class="right"></i>
                            </span>
                        </h2>
                        <p><?=$block['reservation-text'];?></p>
                        <div class="large-12 columns text-center">
                            <a target="_blank" href="<?=$block['reservation-target'];?>" class="btn red"><?=$block['reservation-label'];?></a>
                        </div>
                    </div>
                </div>

            </section>

	    <?php endif;?>
    <?php endforeach;?>
</div>
<?php get_footer();?>
