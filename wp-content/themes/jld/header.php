<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js" ng-app="JLDApp">
<head>
        <meta name="google-site-verification" content="kpVV3aX2eUox_K8JZdr028AX0h-MoKpyWJzfM4Xxz_M" />
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
   	<meta name="google-site-verification" content="8hZQ97uLn9hJqSXmad0m2ZBYyyJga4cyB8_Dtt0XxhQ" />
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<script src="<?=get_template_directory_uri();?>/scripts/vendor/modernizr.js"></script>
	<?php wp_head(); ?>
    <style>
        @media only screen and (max-width: 40em) {
            #homepage-promotions {margin-bottom:47px;}
            #homepage-promotions .dots-container {position: absolute; bottom: -40px; z-index: 3;}
        }
        .newsletter-button {
            width: 200px;
            height: 46px;
            display: block; margin:0 auto; float:none;
            line-height: 46px;
            text-align: center;
        }
        #loyalty-card .loyalty-content .loyalty-image {top:-150px; z-index: -1}
    </style>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NJR39J"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NJR39J');</script>
<!-- End Google Tag Manager -->

<!-- WalkMe -->
<script type="text/javascript">(function() {var walkme = document.createElement('script'); walkme.type = 'text/javascript'; walkme.async = true; walkme.src = 'https://cdn.walkme.com/users/ff03a62a0a9542f2a9a5e549a42811f0/walkme_ff03a62a0a9542f2a9a5e549a42811f0_https.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(walkme, s); window._walkmeConfig =
{smartLoad:true}
; })();</script>
<!-- End WalkMe -->

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NJR39J');</script>
<!-- End Google Tag Manager -->

<!-- PushPushGo -->
<script charset="UTF-8" src="https://cdn.pushpushgo.com/js/599d4bd4c1e02b0015922ad5.js" async="async"></script>
<!-- End PushPushGo -->
</head>




<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NJR39J"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php
if(is_front_page()):
    $frontpage_id = get_option( 'page_on_front' );
?>
<div class="row hide-for-small">
    <img src="<?=get_field('home_bg_image',$frontpage_id);?>" alt="" class="main-bg hide-for-small">
</div>
<?php endif;?>


<?php
$phone = get_field('header-telefon','options');
$phone_target = get_field('header-telefon-target','options');
$online = get_field('header-online','options');
$online_target = get_field('header-online-target','options');
$login = get_field('header-login','options');
$login_target = get_field('header-login-target','options');
$ksk = get_field('header-ksk','options');
$blog = get_field('header-blog','options');
$products = get_field('header-products','options');
$salons = get_field('header_salony','options');
$services = get_field('header_services','options');
?>

<header id="mainheader">
    <div class="row collapse">
        <div class="large-3 medium-4 columns">
            <a href="<?=get_bloginfo('url');?>" title="<?=get_bloginfo();?>" class="logo">
                <img src="<?=get_template_directory_uri();?>/images/logo.svg" width="201" height="65" alt="<?=get_bloginfo();?>">
            </a>
        </div>
        <div class="large-7 medium-3 columns show-for-large-up">
            <ul>
                <?php if($online || $phone):?>
                <li><span>REZERWACJA</span></li>

                <li class="cal"><a href="<?=$online_target;?>" target="_blank"><?=$online;?></a></li>
                <li class="phone"><a href="<?=$phone_target;?>"><?=$phone;?></a></li>
                <?php endif;?>
            </ul>
        </div>
        <div class="large-2 medium-3 columns show-for-large-up my-account">
            <?php if($login):?>
            <a style="display: none" href="<?=$login_target;?>"><?=$login;?></a>
            <?php endif;?>
        </div>
        <div class="hide-for-medium-up burger">
            <div id="hamburger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
    </div>
</header>
<div class="main-navigation">
    <div class="row collapse">
        <nav id="megadropdown">
            <ul>
                <li class="mega-drop">
                    <a href="<?=get_permalink($services->ID);?>" title="<?=$services->post_title;?>">
                        <span><?=$services->post_title;?></span>
                    </a>
                    <div class="mega-drop-inner">
                        <div class="row collapse" data-equalizer>
                            <?php
                                $terms = get_terms('services_category');
                            ?>
                            <?php foreach($terms as $menublock):?>

                                <div data-equalizer-watch class="large-3 columns block <?=$menublock->slug;?>" style="background-image: url('<?=get_field('obrazek_w_menu', 'services_category_' . $menublock->term_id);?>');">
                                    <h3><?=$menublock->description;?></h3>
                                    <h5><?=$menublock->name;?></h5>
                                    <?php $links = get_posts(array('post_type'=>'services', 'services_category'=>$menublock->slug, 'posts_per_page'=>-1));?>
                                    <ul>
                                        <?php foreach ($links as $link):?>

                                            <li><a href="<?=get_permalink($link->ID);?>" title="<?=$link->post_title;?>"><?=$link->post_title;?></a></li>

                                        <?php endforeach;?>
                                    </ul>
                                </div>

                            <?php endforeach;?>
                        </div>
                    </div>
                </li>
                <li>
                    <a href="<?=get_permalink($salons->ID);?>" title="<?=$salons->post_title;?>">
                        <span><?=$salons->post_title;?></span>
                    </a>
                </li>
                <li class="drop-down">
                    <a href="<?=get_permalink($products->ID);?>" title="<?=$products->post_title;?>">
                        <span><?=$products->post_title;?></span>
                    </a>
                    <?php
                        $pagekids = get_pages("child_of=".$products->ID."&sort_column=menu_order");
                    ?>
                    <ul>
                        <?php foreach($pagekids as $prodpage): ?>

                            <li><a href="<?=get_permalink($prodpage->ID);?>" title="<?=$prodpage->post_title;?>"><?=$prodpage->post_title;?></a></li>

                        <?php endforeach;?>
                    </ul>
                </li>
                <li>
                    <a href="<?=get_permalink($ksk->ID);?>" title="<?=$ksk->post_title;?>">
                        <span><?=$ksk->post_title;?></span>
                    </a>
                </li>
                <li>
                    <a href="<?=get_permalink($blog->ID);?>" title="<?=$blog->post_title;?>">
                        <span><?=$blog->post_title;?></span>
                    </a>
                </li>

            </ul>
        </nav>
    </div>
</div>
<div class="row ">
    <div class="large-12 columns">
        <div class="breads" typeof="BreadcrumbList" vocab="http://schema.org/">
		    <?php if(function_exists('bcn_display'))
		    {
			    bcn_display();
		    }?>
        </div>
    </div>
</div>