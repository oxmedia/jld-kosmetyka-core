<?php
/**
 * Template Name: KSK
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header();
?>
    <section id="loyalty-card">
        <div class="row">
            <div class="large-12 columns text-center">
                <h2 class="sectionHeader">
                    <?=get_field('header',$post->ID);?>
                    <span class="under-header-line">
                      <i class="left"></i>
                      <i class="right"></i>
                    </span>
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="large-12 columns">
                <p class="sectionSubHeader">
	                <?=get_field('subheader',$post->ID);?>
                </p>
            </div>
        </div>
        <div class="row" data-equalizer>
            <div data-equalizer-watch class="large-8 columns">
                <div class="loyalty-content">

                    <figure class="loyalty-image">
                        <img src="<?=get_template_directory_uri();?>/images/internet_karta-1.jpg" style="width:230px; height: auto;">
                    </figure>
                    <div class="loyalty-box first">
                        <p><?=get_field('s1_header',$post->ID);?></p>
                        <ul>
                            <li>
                                <img src="<?= get_template_directory_uri(); ?>/images/red-check.svg" alt="">
                                <span><?=get_field('s1_left',$post->ID);?></span>
                            </li>
                            <li>
                                <img src="<?= get_template_directory_uri(); ?>/images/red-check.svg" alt="">
                                <span><?=get_field('s1_right',$post->ID);?></span>
                            </li>
                        </ul>
                        <span class="clearfix"></span>
                        <div class="point-box"><?=get_field('s1_points',$post->ID);?></div>
                    </div>
                    <div class="loyalty-box second">
                        <p><?=get_field('s2_header',$post->ID);?></p>
                        <div class="point-box"><?=get_field('s2_points',$post->ID);?></div>
                        <span class="subtext"><?=get_field('s2_subtext',$post->ID);?></span>
                    </div>
                    <div class="loyalty-box third">
                        <p><?=get_field('s3_header',$post->ID);?></p>
                        <ul class="small-block-grid-1 medium-block-grid-3">
                            <li class="percent">
                                <i class="ksk-percent">
                                    <img src="<?=get_template_directory_uri();?>/images/ksk.percent.svg" alt="">
                                </i>
                                <span>
                                    <?=get_field('s3_rabat',$post->ID);?>
                                </span>
                            </li>
                            <li class="speech">
                                <i class="ksk-speech">
                                    <img src="<?=get_template_directory_uri();?>/images/ksk.speach.svg" alt="">
                                </i>
                                <span>
                                    <?=get_field('s3_promocje',$post->ID);?>
                                </span>
                            </li>
                            <li class="pkt">
                                <i class="ksk-pkt">
                                    <img src="<?=get_template_directory_uri();?>/images/ksk.pkt.svg" alt="">
                                </i>
                                <span>
                                    <?=get_field('s3_konto',$post->ID);?>
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div data-equalizer-watch class="large-4 columns loyalty-sidebar">
                <div class="login-box" style="display: none">
                    <p>zaloguj się</p>

                    <form action="#">
                        <div class="input">
                            <label for="cardnumber">Nr karty</label>
                            <input type="text" id="cardnumber">
                        </div>
                        <div class="input">
                            <label for="password">Hasło</label>
                            <input type="text" id="password">
                        </div>
                        <a href="#" class="forgot-pw">Nie pamiętasz hasła?</a>
                        <button class="btn red">zaloguj się</button>
                    </form>
                </div>
                <div class="loyality-card">
                    <p class="header"><?=get_field('jak_header',$post->ID);?></p>
                    <p><?=get_field('jak_subheader',$post->ID);?></p>
                    <div class="icons">
                        <div class="first-line">
                            <figure>
                                <img src="<?=get_template_directory_uri();?>/images/ksk.laptop.svg" width="54" height="34">
                            </figure>
                            <div class="text">
                                <p>On-line</p>
                            </div>
                        </div>
                        <div class="first-line">
                            <figure>
                                <img src="<?=get_template_directory_uri();?>/images/ksk.drzwi.svg" width="27" height="31">
                            </figure>
                            <div class="text">
                                <p>W salonie</p>
                            </div>
                        </div>
                    </div>
                    <p><?=get_field('bottom_text',$post->ID);?></p>
                </div>
                <a style="position: relative; z-index: 2" class="loyalty-reg" href="<?=get_field('reg_link',$post->ID);?>">Regulamin Karty Stałego Klienta Jean Louis David</a>
            </div>
        </div>
    </section>
    <section id="map" ng-controller="mapsCtrl">
        <div class="row">
            <h2 class="large-push-3 medium-push-1 large-6 medium-10 small-12 columns sectionHeader">
                ZNAJDŹ NAJBLIŻSZY SALON
                <span class="under-header-line">
                    <i class="left"></i>
                    <i class="right"></i>
            </span>
            </h2>
        </div>
		<?php include(locate_template('_partials/mapbox.php'));?>
    </section>
    <style>
        #map {float:left; width:100%;}
        #loyalty-card .loyalty-content .loyalty-box .point-box {
            width: 85.0746268657%;
            min-height: 95px;
            background-color: #eee;
            text-align: center;
            font-family: Frutiger-Medium;
            color: #1a1a1a;
            font-size: 30px;
            display: block;
            margin: 0 auto;
            line-height: 55px;
            padding-top: 20px;
            padding-bottom: 20px;
            height:auto;
        }
        #loyalty-card .loyalty-content .loyalty-box .point-box p {
            margin-bottom: 0;
        }
    </style>
<?php get_footer();?>