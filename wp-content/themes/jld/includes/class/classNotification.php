<?php


class classNotification {

    static function addMessage($message,$mode){
        $hash = md5(time());
		
        $_SESSION['notification'][$hash]['message'] = $message;
		$_SESSION['notification'][$hash]['mode'] = $mode;
    }

    static function flushMessage(){
        if(isset($_SESSION['notification'])){

            $message = array();
            foreach($_SESSION['notification'] as $k=>$arr){
                $message[$k]= new classOutput();
                $message[$k]->message = $arr['message'];
                $message[$k]->mode = $arr['mode'];
            }
            
            unset($_SESSION['notification']);
            return $message;
        }else{
            return array();
        }
    }   
    
}
