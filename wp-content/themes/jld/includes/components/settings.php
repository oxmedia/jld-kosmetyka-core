<?php

/* 
 * THeme settings
 * 
 */

load_theme_textdomain( 'jld' );
add_theme_support( 'title-tag' );
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 1200, 9999 );
set_post_thumbnail_size( 1200, 9999 );


//Image sizes:
add_image_size('news-image-big',276,318,true);
add_image_size('news-image-small',276,214,true);
add_image_size('product-listing',376,376,true);
add_image_size('promo-listing',376,257,true);
add_image_size('service-slide',376,376,true);
add_image_size('why-image',376,257,true);
add_image_size('salon-big',1200,380,true);




register_nav_menus( array(
    'top_menu'      => __( 'Top menu', 'jld' ),
    'cialo_menu'    => __( 'Submenu: Ciało', 'jld' ),
    'twarz_menu'    => __( 'Submenu: Twarz', 'jld' ),
    'dlonie_menu'   => __( 'Submenu: Dłonie i stopy', 'jld' ),
	'dla_niego'     => __( 'Submenu: Dla niego', 'jld' ),
    'footer-menu'   => __( 'Footer menu', 'jld' ),
) );
//HTML5 Support
add_theme_support( 'html5', array(
    'search-form',
    'comment-form',
    'comment-list',
    'gallery',
    'caption',
) );

//SVG Upload
function cc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

//Categories list on post tile
function AB_get_categories($postid,$tax='category') {
	$term_list = wp_get_post_terms($postid, $tax, array("fields" => "all"));
	$cats = [];
	foreach($term_list as $term):
		$cats[] = 'cat-'.$term->slug;
	endforeach;
	if($cats):
		echo implode($cats,' ');
	endif;
}
//Custom pagination
function RC_pagination($pages = '', $range = 2){
	$showitems = ($range * 2)+1;
	global $paged;
	if(empty($paged)) $paged = 1;
	if($pages == '') {
		global $wp_query;
		$pages = $wp_query->max_num_pages;
		if(!$pages)
			$pages = 1;
	}
	if(1 != $pages) {
		echo "<ul class='pagi-navigation'>";
//        if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo;</a>";
		if($paged > 1) echo "<a class='prev' href='".get_pagenum_link($paged - 1)."'><img src='".get_template_directory_uri()."/images/ico/arrow_l_dark.png'></a>";



		for ($i=1; $i <= $pages; $i++)    {
			if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
				echo ($paged == $i)? "<li><a class='current'>".$i."</a></li>":"<li><a  data-animation='".($i - 1)."' href='".get_pagenum_link($i)."' class='inactive' >".$i."</a></li>";
		}
		if ($paged < $pages) echo "<a class='next' href='".get_pagenum_link($paged + 1)."'><img src='".get_template_directory_uri()."/images/ico/arrow_r_dark.png'></a>";
//        if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>&raquo;</a>";


		echo "</ul>\n";
	}
}
//Get menu name
function RC_get_menu_name( $theme_location ) {
	if( ! $theme_location ) return false;

	$theme_locations = get_nav_menu_locations();
	if( ! isset( $theme_locations[$theme_location] ) ) return false;

	$menu_obj = get_term( $theme_locations[$theme_location], 'nav_menu' );
	if( ! $menu_obj ) $menu_obj = false;
	if( ! isset( $menu_obj->name ) ) return false;

	return $menu_obj->name;
}
//GEnerate random string
function RC_random_string($length = 10) {
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return $randomString;
}
//Get excerpt
function getExcerpt($str, $startPos=0, $maxLength=100) {
	if(strlen($str) > $maxLength) {
		$excerpt   = substr($str, $startPos, $maxLength-3);
		$lastSpace = strrpos($excerpt, ' ');
		$excerpt   = substr($excerpt, 0, $lastSpace);
		$excerpt  .= '...';
	} else {
		$excerpt = $str;
	}

	return $excerpt;
}