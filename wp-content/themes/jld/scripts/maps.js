/**
 * Created by WebKoala on 26.11.2016.
 */
var app = angular.module("JLDApp",  ['ngSanitize']);

app.controller("mapsCtrl", function($scope,Salons, $filter) {

    Salons.getAll()
        .then(function (salon) {
            $scope.points = salon.data;
            $scope.filteredMarkers = salon.data;
            $scope.loading = true;
            initMap();
        });

    $scope.breweryFilter = '';
    $scope.$watch("searchText", function(breweryFilter){
        $scope.filteredMarkers = $filter("filter")($scope.points, breweryFilter);
        if (!$scope.filteredMarkers){
            return;
        }
        else {
            placeMarkers();
        }
    });


    $scope.place_type = 'map';
    $scope.setType = function(which) {
        if(which==='map') {
            setTimeout(function() {
                reinitMap();
            },1000);
        }
        $scope.place_type = which;
    };
    $scope.hidePopups = function() {
        $('.box').fadeOut();
        setTimeout(function() {
            centerMap();

        },500);
    };
    var map;
    var coordinates = new google.maps.LatLng(52.03, 19.27);
    var map_options = {
        zoom: 5,
        center: coordinates,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: false,
        scrollwheel: false,
        mapTypeControl: false,
        navigationControl: true,
        streetViewControl: false,

        zoomControlOptions: {
            position: google.maps.ControlPosition.RIGHT_BOTTOM
        },
        styles: [{
            "featureType": "administrative",
            "elementType": "labels.text.fill",
            "stylers": [{"color": "#634d4d"}, {"visibility": "on"}]
        }, {
            "featureType": "landscape",
            "elementType": "all",
            "stylers": [{"color": "#f2f2f2"}]
        }, {"featureType": "poi", "elementType": "all", "stylers": [{"visibility": "off"}]}, {
            "featureType": "road",
            "elementType": "all",
            "stylers": [{"saturation": -100}, {"lightness": 45}]
        }, {
            "featureType": "road",
            "elementType": "geometry",
            "stylers": [{"visibility": "on"}, {"hue": "#ff0000"}]
        }, {
            "featureType": "road",
            "elementType": "geometry.fill",
            "stylers": [{"color": "#ffffff"}]
        }, {
            "featureType": "road",
            "elementType": "geometry.stroke",
            "stylers": [{"color": "#E9283C"}, {"weight": "0.25"}]
        }, {
            "featureType": "road",
            "elementType": "labels.text.fill",
            "stylers": [{"color": "#E9283C"}]
        }, {
            "featureType": "road.highway",
            "elementType": "all",
            "stylers": [{"visibility": "simplified"}]
        }, {
            "featureType": "road.highway",
            "elementType": "geometry.fill",
            "stylers": [{"color": "#ffffff"}, {"lightness": "82"}]
        }, {
            "featureType": "road.highway",
            "elementType": "geometry.stroke",
            "stylers": [{"weight": "10.00"}, {"color": "#cefce3"}]
        }, {
            "featureType": "road.highway",
            "elementType": "labels.text.fill",
            "stylers": [{"color": "#E9283C"}]
        }, {
            "featureType": "road.highway.controlled_access",
            "elementType": "geometry.fill",
            "stylers": [{"color": "#ffffff"}]
        }, {
            "featureType": "road.arterial",
            "elementType": "geometry",
            "stylers": [{"visibility": "on"}]
        }, {
            "featureType": "road.arterial",
            "elementType": "geometry.fill",
            "stylers": [{"color": "#ffffff"}]
        }, {
            "featureType": "road.arterial",
            "elementType": "geometry.stroke",
            "stylers": [{"color": "#cefce3"}]
        }, {
            "featureType": "road.arterial",
            "elementType": "labels.text",
            "stylers": [{"visibility": "simplified"}, {"color": "#E9283C"}]
        }, {
            "featureType": "road.arterial",
            "elementType": "labels.text.fill",
            "stylers": [{"color": "#E9283C"}]
        }, {
            "featureType": "road.arterial",
            "elementType": "labels.icon",
            "stylers": [{"visibility": "off"}]
        }, {
            "featureType": "road.local",
            "elementType": "geometry.fill",
            "stylers": [{"color": "#ffffff"}]
        }, {
            "featureType": "road.local",
            "elementType": "geometry.stroke",
            "stylers": [{"color": "#E9283C"}]
        }, {
            "featureType": "road.local",
            "elementType": "labels",
            "stylers": [{"color": "#E9283C"}, {"visibility": "simplified"}]
        }, {
            "featureType": "road.local",
            "elementType": "labels.text.fill",
            "stylers": [{"color": "#E9283C"}]
        }, {
            "featureType": "transit",
            "elementType": "all",
            "stylers": [{"visibility": "off"}]
        }, {
            "featureType": "transit.line",
            "elementType": "geometry.fill",
            "stylers": [{"color": "#ffffff"}]
        }, {
            "featureType": "transit.line",
            "elementType": "geometry.stroke",
            "stylers": [{"color": "#E9283C"}]
        }, {
            "featureType": "transit.line",
            "elementType": "labels.text.fill",
            "stylers": [{"color": "#E9283C"}]
        }, {
            "featureType": "water",
            "elementType": "all",
            "stylers": [{"color": "#5D5E5E"}, {"visibility": "on"}]
        }, {"featureType": "water", "elementType": "labels.text.fill", "stylers": [{"color": "#ffffff"}]}]
    };

    var marker_green = new google.maps.MarkerImage(adminajax.templateurl + '/images/pin.png', new google.maps.Size(24, 32),         new google.maps.Point(0, 0), new google.maps.Point(11, 11));
    var markers = [];
    google.maps.Map.prototype.panToWithOffset = function(latlng, offsetX, offsetY) {
        var map = this;
        var ov = new google.maps.OverlayView();
        ov.onAdd = function() {
            var proj = this.getProjection();
            var aPoint = proj.fromLatLngToContainerPixel(latlng);
            aPoint.x = aPoint.x+offsetX;
            aPoint.y = aPoint.y+offsetY;
            map.panTo(proj.fromContainerPixelToLatLng(aPoint));
        };
        ov.draw = function() {};
        ov.setMap(this);
    };
    function getPixelPosition(marker) {
        var scale = Math.pow(2, map.getZoom());
        var nw = new google.maps.LatLng(
            map.getBounds().getNorthEast().lat(),
            map.getBounds().getSouthWest().lng()
        );
        var worldCoordinateNW = map.getProjection().fromLatLngToPoint(nw);
        var worldCoordinate = map.getProjection().fromLatLngToPoint(marker.getPosition());
        var pixelOffset = new google.maps.Point(
            Math.floor((worldCoordinate.x - worldCoordinateNW.x) * scale),
            Math.floor((worldCoordinate.y - worldCoordinateNW.y) * scale)
        );

        return pixelOffset;
    }
    function addMarker(point,id,marker) {
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(point.long, point.lat),
            icon: marker,
            map: map
        });
        markers.push(marker);
        var marker_id = id;


        google.maps.event.addListener(marker, "click", function () {
            var box = $('#box-'+marker_id);


            if('box-'+marker_id===$('.box.active').attr('id')) {
                return false;
            }
            $('.box').removeClass('active').fadeOut();
            map.setZoom(15);
            var marker_position = getPixelPosition(marker);
            $('#box-'+marker_id).css({'top': marker_position.y - 45, 'left': marker_position.x + 30});
            map.panToWithOffset(marker.getPosition(), 0, 150);
            box.fadeIn().addClass('active');
        });
        google.maps.event.addListener(map, "center_changed", function () {
            var box = $('#box-'+marker_id);
            var marker_position = getPixelPosition(marker);
            $('#box-'+marker_id).css({'top': marker_position.y - 45, 'left': marker_position.x + 30});
        });
        google.maps.event.addListener(map, "zoom_changed", function () {
            var box = $('#box-'+marker_id);
            var marker_position = getPixelPosition(marker);
            $('#box-'+marker_id).css({'top': marker_position.y - 45, 'left': marker_position.x + 30});
        });
    }
    function placeMarkers() {
        while(markers.length){
            markers.pop().setMap(null);
        }
        var markery = $scope.filteredMarkers.length;
        for (var i = 0; i < markery; i++) {
            addMarker($scope.filteredMarkers[i],i,marker_green);
        }
    }
    function centerMap() {
        var bounds = new google.maps.LatLngBounds();
        for (var i = 0; i < markers.length; i++) {
            bounds.extend(markers[i].getPosition());
        }

        map.fitBounds(bounds);
    }
    function initMap() {
        map = new google.maps.Map(document.getElementById('bigMap'), map_options);
        placeMarkers();
        centerMap();
    }
    function reinitMap() {
        $('#bigMap').html("");
        initMap();
    }
});
app.service('Salons', function($http,$q) {
    this.getAll = function(params) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: adminajax.ajaxurl,
            params: {
                action:'getsalons'
            },
        }).then(function(data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    };
});