"use strict";
var JLD = {};
jQuery(document).ready(function () {
    $(document).foundation();

    jQuery.extend(window.JLD, {
        init: function () {
            JLD.homePagePromoSlider();
            JLD.serviceWhy();
            JLD.singleService();
            JLD.accordions();
            JLD.objectList();
            JLD.mobileMenu();
            JLD.desktopMenu();
            JLD.mouseClick();
            JLD.homemap();
            JLD.loadMore();
            JLD.contactMap();
            JLD.servicesCarousel();
        },
        servicesCarousel: function () {
            jQuery('.services-carousel .slides').owlCarousel({
                items:3,
                loop:true,
                margin:0,
                responsive:{
                    0:{
                        items:1,
                        nav:true,
                        center:true,
                        pagination : true,
                        dots:true,
                        navContainer: '.services-carousel .nav-container',
                        dotsContainer: '.services-carousel .dots-container'
                    },
                    641:{
                        items:2,
                        nav:false,
                        dots:false
                    },
                    1024:{
                        items:3,
                        nav:true,
                        dots:false,
                        navContainer: '.services-carousel .nav-container',
                        dotsContainer: '.services-carousel .dots-container'
                    }
                }
            });
        },
        contactMap: function() {

var mapbox  = $('#contact-map');
            if(mapbox.length) {

                var coordinates = new google.maps.LatLng(52.03, 19.27);
                var map_options = {
                    zoom: 15,
                    center: coordinates,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    disableDefaultUI: false,
                    scrollwheel: false,
                    mapTypeControl: false,
                    navigationControl: true,
                    streetViewControl: false,

                    zoomControlOptions: {
                        position: google.maps.ControlPosition.RIGHT_BOTTOM
                    },
                    styles: [{
                        "featureType": "administrative",
                        "elementType": "labels.text.fill",
                        "stylers": [{"color": "#634d4d"}, {"visibility": "on"}]
                    }, {
                        "featureType": "landscape",
                        "elementType": "all",
                        "stylers": [{"color": "#f2f2f2"}]
                    }, {"featureType": "poi", "elementType": "all", "stylers": [{"visibility": "off"}]}, {
                        "featureType": "road",
                        "elementType": "all",
                        "stylers": [{"saturation": -100}, {"lightness": 45}]
                    }, {
                        "featureType": "road",
                        "elementType": "geometry",
                        "stylers": [{"visibility": "on"}, {"hue": "#ff0000"}]
                    }, {
                        "featureType": "road",
                        "elementType": "geometry.fill",
                        "stylers": [{"color": "#ffffff"}]
                    }, {
                        "featureType": "road",
                        "elementType": "geometry.stroke",
                        "stylers": [{"color": "#E9283C"}, {"weight": "0.25"}]
                    }, {
                        "featureType": "road",
                        "elementType": "labels.text.fill",
                        "stylers": [{"color": "#E9283C"}]
                    }, {
                        "featureType": "road.highway",
                        "elementType": "all",
                        "stylers": [{"visibility": "simplified"}]
                    }, {
                        "featureType": "road.highway",
                        "elementType": "geometry.fill",
                        "stylers": [{"color": "#ffffff"}, {"lightness": "82"}]
                    }, {
                        "featureType": "road.highway",
                        "elementType": "geometry.stroke",
                        "stylers": [{"weight": "10.00"}, {"color": "#cefce3"}]
                    }, {
                        "featureType": "road.highway",
                        "elementType": "labels.text.fill",
                        "stylers": [{"color": "#E9283C"}]
                    }, {
                        "featureType": "road.highway.controlled_access",
                        "elementType": "geometry.fill",
                        "stylers": [{"color": "#ffffff"}]
                    }, {
                        "featureType": "road.arterial",
                        "elementType": "geometry",
                        "stylers": [{"visibility": "on"}]
                    }, {
                        "featureType": "road.arterial",
                        "elementType": "geometry.fill",
                        "stylers": [{"color": "#ffffff"}]
                    }, {
                        "featureType": "road.arterial",
                        "elementType": "geometry.stroke",
                        "stylers": [{"color": "#cefce3"}]
                    }, {
                        "featureType": "road.arterial",
                        "elementType": "labels.text",
                        "stylers": [{"visibility": "simplified"}, {"color": "#E9283C"}]
                    }, {
                        "featureType": "road.arterial",
                        "elementType": "labels.text.fill",
                        "stylers": [{"color": "#E9283C"}]
                    }, {
                        "featureType": "road.arterial",
                        "elementType": "labels.icon",
                        "stylers": [{"visibility": "off"}]
                    }, {
                        "featureType": "road.local",
                        "elementType": "geometry.fill",
                        "stylers": [{"color": "#ffffff"}]
                    }, {
                        "featureType": "road.local",
                        "elementType": "geometry.stroke",
                        "stylers": [{"color": "#E9283C"}]
                    }, {
                        "featureType": "road.local",
                        "elementType": "labels",
                        "stylers": [{"color": "#E9283C"}, {"visibility": "simplified"}]
                    }, {
                        "featureType": "road.local",
                        "elementType": "labels.text.fill",
                        "stylers": [{"color": "#E9283C"}]
                    }, {
                        "featureType": "transit",
                        "elementType": "all",
                        "stylers": [{"visibility": "off"}]
                    }, {
                        "featureType": "transit.line",
                        "elementType": "geometry.fill",
                        "stylers": [{"color": "#ffffff"}]
                    }, {
                        "featureType": "transit.line",
                        "elementType": "geometry.stroke",
                        "stylers": [{"color": "#E9283C"}]
                    }, {
                        "featureType": "transit.line",
                        "elementType": "labels.text.fill",
                        "stylers": [{"color": "#E9283C"}]
                    }, {
                        "featureType": "water",
                        "elementType": "all",
                        "stylers": [{"color": "#E9283C"}, {"visibility": "on"}]
                    }, {"featureType": "water", "elementType": "labels.text.fill", "stylers": [{"color": "#ffffff"}]}]
                };

                var map = new google.maps.Map(document.getElementById('contact-map'), map_options);
                var marker = new google.maps.MarkerImage(adminajax.templateurl + '/images/pin.png', new google.maps.Size(24, 32), new google.maps.Point(0, 0), new google.maps.Point(11, 11));
                new google.maps.Marker({
                    position: new google.maps.LatLng(mapbox.data('lng'), mapbox.data('lat')),
                    icon: marker,
                    map: map
                });

            }
        },
        homePagePromoSlider: function () {
            jQuery('.promotions-carousel .slides').owlCarousel({
                items:3,
                loop:true,
                margin:0,
                responsive:{
                    0:{
                        items:1,
                        nav:true,
                        center:true,
                        pagination : true,
                        dots:true,
                        navContainer: '.nav-container',
                        dotsContainer: '.dots-container'
                    },
                    641:{
                        items:2,
                        nav:false,
                        dots:false
                    },
                    1024:{
                        items:3,
                        nav:true,
                        dots:false,
                        navContainer: '.nav-container',
                        dotsContainer: '.dots-container'
                    }
                }
            });
        },
        serviceWhy: function () {
            jQuery('.service-why').owlCarousel({
                items:3,
                loop:true,
                margin:0,
                responsive:{
                    0:{
                        items:1,
                        nav:true,
                        center:true,
                        pagination : true,
                        dots:true,
                        navContainer: '.nav-container-why',
                        dotsContainer: '.dots-container-why'
                    },
                    641:{
                        items:2,
                        nav:false,
                        dots:false,
                        navContainer: '.nav-container-why',
                        dotsContainer: '.dots-container-why'
                    },
                    1024:{
                        items:3,
                        nav:false,
                        dots:false,
                        navContainer: '.nav-container-why',
                        dotsContainer: '.dots-container-why'
                    }
                }
            });
        },
        singleService: function () {
            jQuery('.service-carousel').owlCarousel({
                items:1,
                loop:true,
                margin:0,
                nav:true,
                center:true,
                pagination : true,
                dots:true,
                navContainer: '.nav-container',
                dotsContainer: '.dots-container'
            });
        },
        accordions: function() {
            jQuery(".accordion div").show();
            setTimeout("jQuery('.accordion div').slideToggle('slow');", 1000);
            jQuery(".accordion h3").click(function () {
                jQuery(this).next(".pane").slideToggle("slow").siblings(".pane:visible").slideUp("slow");
                jQuery(this).toggleClass("current");
                jQuery(this).siblings("h3").removeClass("current");
                return false;
            });
        },
        objectList: function() {
            jQuery(".object .details").hide();
            jQuery('.dostepnosc a').click(function(){
                var container = jQuery(this).parent().parent();
                container.children('.details').slideToggle();
                jQuery(this).toggleClass('active');
                return false;
            })
        },
        desktopMenu: function() {
        },
        mobileMenu: function() {
            $('#hamburger').click(function(){
                var _this = $(this);
                var _menuwrap = $('#megadropdown');
                if(_this.hasClass('open')) {
                    _this.removeClass('open');
                    _menuwrap.slideUp();
                }
                else {
                    _this.addClass('open');
                    _menuwrap.slideDown();
                }
                return false;
            });
            $('.mega-drop > a').click(function() {

                var _this = $(this),
                    _md = $('.mega-drop-inner');

                if(_this.hasClass('open')) {
                    _this.removeClass('open');
                    _md.slideUp();
                }
                else {
                    _this.addClass('open');
                    _md.slideDown();
                }
                return false;
            });
            $('.drop-down > a').click(function() {

                var _this = $(this),
                    _md = $('.drop-down > ul');

                if(_this.hasClass('open')) {
                    _this.removeClass('open');
                    _md.slideUp();
                }
                else {
                    _this.addClass('open');
                    _md.slideDown();
                }
                return false;
            });
        },
        mouseClick: function() {

        },
        homemap:function() {

        },
        loadMore: function(){

            if($('#newsContainer').length) {
                let _spinner = $('#news-spinner');
                let _btn = $('#loadMoreNews');
                var offset = parseInt(_btn.data('offset'));



                _spinner.fadeIn();
                JLD._loadMore(offset, JLD.insertNews);
                var offset = parseInt(_btn.data('offset') + 6);
                _btn.data('offset',offset);
                _spinner.fadeOut();

                _btn.click(function(e){
                    e.preventDefault();
                    _spinner.fadeIn();
                    var offset = parseInt(_btn.data('offset'));
                    JLD._loadMore(offset, JLD.insertNews);

                    console.log(offset);
                    _btn.data('offset',offset + 6);
                    _spinner.fadeOut();

                });
            }
        },
        insertNews:function(res){
            console.log(res);
            if(res == 'error'){
                alert('Loading error. Please try again later.');
                return;
            }

            if(res.counter<6){
                $('#loadMoreNews').fadeOut();
            }

            $('#newsContainer ul.left').append(res.lcol);
            $('#newsContainer ul.right').append(res.rcol);
            $('#newsContainer ul.mid').append(res.mcol);
        },
        _loadMore: function(offset,callback) {
            if (typeof callback !== 'function') {
                callback = false;
            }
            $.ajax({
                type: "POST",
                url: adminajax.ajaxurl,
                data: {'post-type': 'news', 'offset': offset, 'action': 'load_more'},
                success: function (res) {
                    res = JSON.parse(res);
                    callback(res);
                },
                error: function(){
                    callback('error');
                }
            });
        }
    });

    JLD.init();
});