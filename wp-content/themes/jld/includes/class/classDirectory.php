<?php

class classDirectory {
	
	static function getFileList($dir){
		$files = scandir($dir);
		$return = array();
		foreach($files as $file){
			if($file != '.' && $file != '..'){
				$return[] = $file;
			}
		}
		
		return $return;
	}
	
	
}

