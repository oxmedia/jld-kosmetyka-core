<?php

/* 
 * Plik w którym dodajemy post_type i taxiomie
 * Poniżej szablony do kopiowania
*/
// Members:
register_post_type( 'salon', array(
	'labels'             => array(
		'name'               => __( 'Salon' ),
		'singular_name'      => __( 'Salony' ),
		'add_new'            => __( 'Dodaj nowy' ),
		'add_new_item'       => __( 'Dodaj nowy salon' ),
		'edit_item'          => __( 'Edytuj salon' ),
		'new_item'           => __( 'Nowy salon' ),
		'all_items'          => __( 'Wszystkie salony' ),
		'view_item'          => __( 'Zobacz salony' ),
		'search_items'       => __( 'Szukaj Salonu' ),
		'not_found'          => __( 'Nie znaleziono salonu' ),
		'not_found_in_trash' => __( 'Nie znaleziono salonu w koszu' )
	),
	'public'             => true,
	'publicly_queryable' => true,
	'show_ui'            => true,
	'show_in_menu'       => true,
	'query_var'          => true,
	'rewrite' => array('slug' => 'salony','with_front' => false),
	'capability_type'    => 'post',
	'has_archive'        => false,
	'hierarchical'       => false,
	'menu_position'      => null,
	'menu_icon'          => '',
	'supports'           => array(
		'title',
		'editor',
		'thumbnail',
//		'page-attributes'
	)
) );
register_post_type( 'promotions', array(
	'labels'             => array(
		'name'               => __( 'Promocje' ),
		'singular_name'      => __( 'Promocje' ),
		'add_new'            => __( 'Dodaj nową' ),
		'add_new_item'       => __( 'Dodaj nową promocję' ),
		'edit_item'          => __( 'Edytuj promocję' ),
		'new_item'           => __( 'Nowa promocja' ),
		'all_items'          => __( 'Wszystkie promocje' ),
		'view_item'          => __( 'Zobacz promocję' ),
		'search_items'       => __( 'Szukaj promocji' ),
		'not_found'          => __( 'Nie znaleziono promocji' ),
		'not_found_in_trash' => __( 'Nie znaleziono promocji w koszu' )
	),
	'public'             => true,
	'publicly_queryable' => true,
	'show_ui'            => true,
	'show_in_menu'       => true,
	'query_var'          => true,
	'rewrite'            => array('slug' => 'promocje','with_front' => false),
	'capability_type'    => 'page',
	'has_archive'        => false,
	'hierarchical'       => false,
	'menu_position'      => null,
	'menu_icon'          => '',
	'supports'           => array(
		'title',
		'editor',
		'thumbnail',
//		'page-attributes'
	)
) );
register_post_type( 'products', array(
	'labels'             => array(
		'name'               => __( 'Produkty' ),
		'singular_name'      => __( 'Produkty' ),
		'add_new'            => __( 'Dodaj nowy' ),
		'add_new_item'       => __( 'Dodaj nowy produkt' ),
		'edit_item'          => __( 'Edytuj produkt' ),
		'new_item'           => __( 'Nowy produkt' ),
		'all_items'          => __( 'Wszystkie produkty' ),
		'view_item'          => __( 'Zobacz produkty' ),
		'search_items'       => __( 'Szukaj produktu' ),
		'not_found'          => __( 'Nie znaleziono produktu' ),
		'not_found_in_trash' => __( 'Nie znaleziono produktu w koszu' )
	),
	'public'             => true,
	'publicly_queryable' => true,
	'show_ui'            => true,
	'show_in_menu'       => true,
	'query_var'          => true,
	'rewrite' => array('slug' => 'produkt','with_front' => false),
	'capability_type'    => 'post',
	'has_archive'        => true,
	'hierarchical'       => false,
	'menu_position'      => null,
	'menu_icon'          => '',
	'supports'           => array(
		'title',
		'editor',
		'thumbnail',
		'page-attributes'
	)
) );
register_taxonomy( 'products_category', array( 'products' ), array(
	'labels'             => array(
		'name'               => __( 'Kategoria' ),
		'singular_name'      => __( 'Kategorie' ),
		'add_new'            => __( 'Dodaj nową' ),
		'add_new_item'       => __( 'Dodaj nową kategorię' ),
		'edit_item'          => __( 'Edytuj Kategorię' ),
		'new_item'           => __( 'Nowa kategoria' ),
		'all_items'          => __( 'Wszystkie kategorie' ),
		'view_item'          => __( 'Zobacz kategorie' ),
		'search_items'       => __( 'Szukaj kategorii' ),
		'not_found'          => __( 'Nie znaleziono kategorii' ),
		'not_found_in_trash' => __( 'Nie znaleziono kategorii w koszu ' )
	),
	'public'             => true,
	'publicly_queryable' => true,
	'show_ui'            => true,
	'show_in_menu'       => true,
	'query_var'          => true,
	'rewrite'            => false,
	'hierarchical'       => true
) );
function product_add_taxonomy_filters() {
	global $typenow;

	$taxonomies = array( 'products_category' );

	if ( $typenow == 'products' ) {
		foreach ( $taxonomies as $tax_slug ) {
			$tax_obj  = get_taxonomy( $tax_slug );
			$tax_name = $tax_obj->labels->name;
			$terms    = get_terms( $tax_slug );
			if ( count( $terms ) > 0 ) {
				echo '<select name="' . $tax_slug . '" id="' . $tax_slug . '" class="postform">';
				echo '<option value="0">Zobacz wszystkie kategorie</option>';
				foreach ( $terms as $term ) {
					echo '<option value="' . $term->slug . '"' . ( $_GET[ $tax_slug ] == $term->slug ? ' selected="selected"' : '' ) . '>' . $term->name . ' (' . $term->count . ')</option>';
				}
				echo '</select>';
			}
		}
	}
}



register_post_type( 'services', array(
	'labels'             => array(
		'name'               => __( 'Usługi' ),
		'singular_name'      => __( 'Usługi' ),
		'add_new'            => __( 'Dodaj nową' ),
		'add_new_item'       => __( 'Dodaj nową usługę' ),
		'edit_item'          => __( 'Edytuj usługę' ),
		'new_item'           => __( 'Nowa usługa' ),
		'all_items'          => __( 'Wszystkie usługi' ),
		'view_item'          => __( 'Zobacz usługi' ),
		'search_items'       => __( 'Szukaj usługi' ),
		'not_found'          => __( 'Nie znaleziono usługi' ),
		'not_found_in_trash' => __( 'Nie znaleziono usługi w koszu' )
	),
	'public'             => true,
	'publicly_queryable' => true,
	'show_ui'            => true,
	'show_in_menu'       => true,
	'query_var'          => true,
	'rewrite' => array('slug' => 'usluga','with_front' => false),
	'capability_type'    => 'post',
	'has_archive'        => true,
	'hierarchical'       => false,
	'menu_position'      => null,
	'menu_icon'          => '',
	'supports'           => array(
		'title',
		'editor',
		'thumbnail',
		'page-attributes'
	)
) );
register_taxonomy( 'services_category', array( 'services' ), array(
	'labels'             => array(
		'name'               => __( 'Kategoria' ),
		'singular_name'      => __( 'Kategorie' ),
		'add_new'            => __( 'Dodaj nową' ),
		'add_new_item'       => __( 'Dodaj nową kategorię' ),
		'edit_item'          => __( 'Edytuj Kategorię' ),
		'new_item'           => __( 'Nowa kategoria' ),
		'all_items'          => __( 'Wszystkie kategorie' ),
		'view_item'          => __( 'Zobacz kategorie' ),
		'search_items'       => __( 'Szukaj kategorii' ),
		'not_found'          => __( 'Nie znaleziono kategorii' ),
		'not_found_in_trash' => __( 'Nie znaleziono kategorii w koszu ' )
	),
	'public'             => true,
	'publicly_queryable' => true,
	'show_ui'            => true,
	'show_in_menu'       => true,
	'query_var'          => true,
	'rewrite'            => false,
	'hierarchical'       => true
) );





register_post_type('blog', array(
    'labels'             => array(
        'name'               => __( 'Blog' ),
        'singular_name'      => __( 'Blog' ),
        'add_new'            => __( 'Dodaj nowy' ),
        'add_new_item'       => __( 'Dodaj nowy wpis' ),
        'edit_item'          => __( 'Edytuj wpis' ),
        'new_item'           => __( 'Nowy wpis' ),
        'all_items'          => __( 'Wszystkie wpisy' ),
        'view_item'          => __( 'Zobacz wpisy' ),
        'search_items'       => __( 'Szukaj wpisu' ),
        'not_found'          => __( 'Nie znaleziono wpisu' ),
        'not_found_in_trash' => __( 'Nie znaleziono wpisu w koszu' )
    ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite' => array('slug' => 'blog','with_front' => false),
    'capability_type'    => 'post',
    'has_archive'        => false,
    'hierarchical'       => false,
    'menu_position'      => null,
    'menu_icon'          => '',
    'supports'           => array(
        'title',
        'editor',
        'thumbnail',
    )
) );





add_action( 'restrict_manage_posts', 'product_add_taxonomy_filters' );
if ( function_exists( 'acf_add_options_page' ) ) {

	acf_add_options_page( array(
		'page_title' => 'Ustawienia szablonu',
		'menu_title' => 'Ustawienia szablonu',
		'menu_slug'  => 'theme-general-settings',
		'capability' => 'edit_posts',
	) );

}